<?php
	// DEFININDO SE ESTÁ EM PRODUÇÃO
	$ativoProducao = 0;
	$idParceiroGlobal = null;

	$CONFIG_DEVELOPER = gethostname();
	
	global 	$host, 
			$user, 
			$pass, 
			$dbPortal, 
			$ativoProducao,
			
			$url_consulta_saldo,
			$url_compra_giftcard,
			$url_compra_recarga,
			$url_compra_vale_compra,
			$url_validar_senha,
			$url_categoria,
			$url_produto_usuario_disponivel,
			$url_comprar_registrar,
			$url_parceiros_ativo_home,
			$url_produtos_ativo_home,
			$url_produtos_ativos,
			$url_usuario_endereco,
			$url_produto_buscar,	

			$ativoRegressiva,
			$ativoLiberarCompra,
			$formatoDataBanco,
			$tipoConexao,
			$idParceiroGlobal,
			// AUTENTICAÇÃO
			$token,
			$ativo_uso_app;

	$token = (isset($_REQUEST['token'])) ? $_REQUEST['token'] : null; 
	$ativo_uso_app = (isset($_REQUEST['ativo_uso_app'])) ? $_REQUEST['ativo_uso_app'] : 0;  
		
	
	date_default_timezone_set('America/Manaus');
	
	$ativoCategorias 			=	0;
	$ativoContagemRegressiva	=	1;
	$ativoSlidePrincipal		=	1;
	$ativoBannerPrincipal		=	0;
	$ativoCategoriaPorValor		=	1;
	$ativoProdutoPrincipal		=	1;
	$ativoProdutoParceiros		=	0;
	$ativoParceirosGrupo		=	1;
	$ativoOfertasProduto		=	1;
	$ativoOfertasProdutosParceiros  =   1;
	$ativoParceirosHome			= 1;
	//
	$tipoApresentacaoGrid		=   'grade'; /*	grade ; rolagem*/
	

	if ($ativoProducao) {
		//$url_principal = "https://apiapp.trocados.com.br:8080/app/v2/";
		$url_principal = "https://apiappv3.trocados.com.br:8080/app/v2/";
		$url_home = "https://prod-mercadotrocados-dot-new-aplicativo-trocados.uc.r.appspot.com/index.php?token="; //atual
		//$url_principal = "http://localhost:8080/app/v2/";
		//$url_home = "http://localhost/trocados/mercado-trocados/trocados-marketplace-php/index.php?token=";
		//$url_home = "http://localhost/MKPLACE%20TROCADOS%20-%20LOJA%20ATUAL/trocados-marketplace-php/index.php?token="; //Gabriel
	} else {
		//$url_principal = "https://apiapp-dot-new-aplicativo-trocados.uc.r.appspot.com/app/v2/";		
		//$url_home = "https://homol-mercadotrocados-dot-new-aplicativo-trocados.uc.r.appspot.com/index.php?token=";
		$url_principal = "http://localhost:8080/app/v2/";
		$url_home = "http://localhost/trocados/mercado-trocados/trocados-marketplace-php/index.php?token="; //Diandro
		
		
	}	

	if ( $CONFIG_DEVELOPER == 'DESKTOP-ET1IDTV') {

		// DIANDRO MÁQUINA DE FORA
		if (!$ativoProducao){
			$host = "35.238.153.139"; // PRODUÇÃO
		} else {
			$host = "35.223.4.246"; // HOMOLOGAÇÃO
		}
		$user = "diandro";
		$pass = "trocados2020";
		$dbPortal= "mkplace";    
		$pastaTemp= "Temp/"; 		
		$ativoRegressiva = 1;
		$formatoDataBanco = "Y-d-m H:i:s";
		
		$ativoLiberarCompra = 1;
		$tipoConexao = 0;
		//$url_principal = "http://localhost:8080/app/v2/";
		$url_home = "http://localhost/trocados/mercado-trocados/trocados-marketplace-php/index.php?token=";
		
									

	} else if ( $CONFIG_DEVELOPER == 'DESKTOP-H571B0E') {
		// DIANDRO VM
		$host = "35.238.153.139";
		$user = "diandro";
		$pass = "trocados2020";
		$dbPortal= "mkplace";    
		$pastaTemp= "Temp/"; 		
		$ativoRegressiva = 1;
		$formatoDataBanco = "Y-d-m H:i:s";
		
		$ativoLiberarCompra = 0;
		$tipoConexao = 0;
									

	} else if ( $CONFIG_DEVELOPER == 'DESKTOP-GJETCPB') {
		// GABRIEL
		if ($ativoProducao){
			$host = "35.238.153.139"; // PRODUÇÃO
		} else {
			$host = "35.223.4.246"; // HOMOLOGAÇÃO
		}
		$user = "diandro";
		$pass = "trocados2020";
		$dbPortal= "mkplace";    
		$pastaTemp= "Temp/"; 		
		$ativoRegressiva = 1;
		$formatoDataBanco = "Y-d-m H:i:s";

		$ativoLiberarCompra = 0;
		$tipoConexao = 0;

	} else {
		// PRODUÇÃO
		$user = getenv('CLOUDSQL_USER');
		$pass = getenv('CLOUDSQL_PASSWORD');
		$host = getenv('CLOUDSQL_DSN');
		$dbPortal = getenv('CLOUDSQL_DB');	
		$pastaTemp= "Temp/"; 		
		$ativoRegressiva = 1;
		$formatoDataBanco = "Y-d-m H:i:s";

		$ativoLiberarCompra = 1;
		$tipoConexao = 1;

			
	}	

	$url_consulta_saldo 					= 	$url_principal."saldo";
	$url_compra_giftcard 					=	$url_principal."adquire_black";
	$url_compra_recarga 					= 	$url_principal."adquire_black";
	$url_compra_vale_compra 				= 	$url_principal."gerar_voucher_compra";
	$url_validar_senha						=	$url_principal."validar_senha";
	$url_categoria							=	$url_principal."categoria";	
	$url_produto_usuario_disponivel			=	$url_principal."produto_usuario_disponivel";	
	$url_comprar_registrar					=	$url_principal."compra_registrar";	
	$url_parceiros_ativo_home				=	$url_principal."parceiros_ativo_home";	
	$url_produtos_ativo_home				=	$url_principal."produtos_ativo_home";	
	$url_produtos_ativos					=	$url_principal."produtos_ativos";	
	$url_usuario_endereco					=	$url_principal."usuario_endereco";	
	$url_produto_buscar						=	$url_principal."produto_buscar";	
	
	
	