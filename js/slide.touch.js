var breakpoint = {
    // Small screen / phone
    sm: 576,
    // Medium screen / tablet
    md: 768,
    // Large screen / desktop
    lg: 992,
    // Extra large screen / wide desktop
    xl: 1200
  };
  
  // page slider
  $('.slick').slick({
    autoplay: true,
    autoplaySpeed: 500000,
    draggable: true,
    infinite: true,
    dots: false,
    arrows: false,
    speed: 1000,
    mobileFirst: true,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
      {
        breakpoint: breakpoint.sm,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4
        }
      },
      {
        breakpoint: breakpoint.md,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 5
        }
      },
      {
        breakpoint: breakpoint.lg,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 5
        }
      },
      {
        breakpoint: breakpoint.xl,
        settings: {
          slidesToShow: 6,
          slidesToScroll: 6
        }
      }
    ]
  });
  

  
  // page slider
  $('.slick-1').slick({
    autoplay: true,
    autoplaySpeed: 10000,
    draggable: true,
    infinite: true,
    dots: false,
    arrows: false,
    speed: 1000,
    mobileFirst: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: breakpoint.sm,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: breakpoint.md,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: breakpoint.lg,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: breakpoint.xl,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.slick-2').slick({
    autoplay: false,
    autoplaySpeed: 10000,
    draggable: true,
    infinite: true,
    dots: false,
    arrows: false,
    speed: 300,
    mobileFirst: true,
    slidesToShow: 2.1,
    slidesToScroll: 1,
    centerMode: false,
    swipeToSlide:true,
    rtl:false,
    responsive: [
      {
        breakpoint: breakpoint.sm,
        settings: {
          slidesToShow: 2.1,
          slidesToScroll: 1,
          centerMode: false,
          swipeToSlide:true,
        }
      },
      {
        breakpoint: breakpoint.md,
        settings: {
          slidesToShow: 2.1,
          slidesToScroll: 1,
          centerMode: false,
          swipeToSlide:true,
        }
      },
      {
        breakpoint: breakpoint.lg,
        settings: {
          slidesToShow: 2.1,
          slidesToScroll: 4,
          centerMode: false,
          swipeToSlide:true,
        }
      },
      {
        breakpoint: breakpoint.xl,
        settings: {
          slidesToShow: 2.1,
          slidesToScroll: 5,
          centerMode: false,
          swipeToSlide:true,
        }
      }
    ]
  });
  


    // page slider
    $('.slick-3').slick({
      autoplay: true,
      autoplaySpeed: 10000,
      draggable: true,
      infinite: true,
      dots: false,
      arrows: false,
      speed: 1000,
      mobileFirst: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: breakpoint.sm,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: breakpoint.md,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: breakpoint.lg,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: breakpoint.xl,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });
    

    // page slider
  $('.slick-6').slick({
    autoplay: true,
    autoplaySpeed: 500000,
    draggable: true,
    infinite: true,
    dots: false,
    arrows: false,
    speed: 1000,
    mobileFirst: true,
    slidesToShow: 6,
    slidesToScroll: 6,
    responsive: [
      {
        breakpoint: breakpoint.sm,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 5
        }
      },
      {
        breakpoint: breakpoint.md,
        settings: {
          slidesToShow: 6,
          slidesToScroll: 6
        }
      },
      {
        breakpoint: breakpoint.lg,
        settings: {
          slidesToShow: 6,
          slidesToScroll: 6
        }
      },
      {
        breakpoint: breakpoint.xl,
        settings: {
          slidesToShow: 7,
          slidesToScroll: 7
        }
      }
    ]
  });

   // page slider
   $('.slick-5').slick({
    autoplay: true,
    autoplaySpeed: 500000,
    draggable: true,
    infinite: true,
    dots: false,
    arrows: false,
    speed: 1000,
    mobileFirst: true,
    slidesToShow: 5,
    slidesToScroll: 5,
    responsive: [
      {
        breakpoint: breakpoint.sm,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4
        }
      },
      {
        breakpoint: breakpoint.md,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 5
        }
      },
      {
        breakpoint: breakpoint.lg,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 5
        }
      },
      {
        breakpoint: breakpoint.xl,
        settings: {
          slidesToShow: 6,
          slidesToScroll: 6
        }
      }
    ]
  });

