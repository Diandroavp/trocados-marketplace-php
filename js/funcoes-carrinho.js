

function getCarrinho(token) {

    var dadosParametro = {
        nomeFuncao : 'carrinho',
        token: token
      };
      
      $.ajax({
      url: './services/carrinho/carrinho.service.php',
        dataType: "json",
        data: dadosParametro,
        success: function (response) {  
          if (response){
            $('#valorCarrinhoTotal').html(response[0].valorTotal);
            $('#quantidadeTotal').html('('+ response[0].quantidadeTotal + ')');            
            $('#idParceiroCarrinho').val(response[0].idParceiro);            
            if (response[0].ativoCarrinho){          
              $('#carrinho').css("display", "block");
            }
          } else {
            $('#carrinho').css("display", "none");
          }
          
        }
  
    })
  
  }
  
function addProduto(idProduto, idParceiro, token, nomeImagem, nomeProduto) {    
    var dadosParametro = {
        nomeFuncao : 'addProduto',        
        idProduto: idProduto,
        idParceiro: idParceiro,
        token: token
      };

      Swal.fire({
        title: nomeProduto,
        text: "Adicionar ao carrinho?",
        inputAttributes: {
            autocapitalize: 'off'
        },
        showCancelButton: true,
        showConfirmButton: true,
        confirmButtonText: 'Look up',
        imageUrl: nomeImagem,
        imageWidth: 200,
        imageHeight: 200,
        showCancelButton: true,
        confirmButtonColor: '#33CB00',
        cancelButtonColor: '#ccc',
        confirmButtonText: 'Sim, Adicionar!',
        cancelButtonText: 'Cancelar',
        preConfirm: () => {        
            $.ajax({
                url: './services/produto/produto.service.php',
                dataType: "json",
                data: dadosParametro,
                success: function (response) {  
                    if(response[0].ativoRetorno == 1){
                        getCarrinho(token);
                    } else {
                        Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        confirmButtonColor: '#33CB00',
                        text: response[0].textoRetorno                
                    })   
                    }
                    }  
                }) 
        },
        allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
        
        })
              
}