
<script>
    /** Envia Formulário*/
    function enviaFormularioSimples(form, autenticacao = 1){        
        if (autenticacao){
            adicionarAutenticacao(form);
        }        
        document.getElementById(form).submit();   
        return true;    
    }


    function adicionarAutenticacao(form){
        var token = '<?=(isset($_REQUEST['token'])) ? $_REQUEST['token'] : null?>';
        var ativo_uso_app = '<?=(isset($_REQUEST['ativo_uso_app'])) ? $_REQUEST['ativo_uso_app'] : null?>';

        var formulario = document.getElementById(form);

        if (token != '') {
            var token_elemento = document.createElement("input");
            token_elemento.setAttribute('type', 'hidden');
            token_elemento.setAttribute('name', 'token');
            token_elemento.setAttribute('value', token);

            formulario.appendChild(token_elemento);
        }
        
        if (ativo_uso_app != '') {
            var uso_app = document.createElement("input");
                uso_app.setAttribute('type', 'hidden');
                uso_app.setAttribute('name', 'ativo_uso_app');
                uso_app.setAttribute('value', ativo_uso_app);

            formulario.appendChild(uso_app);
        }
    }
</script>