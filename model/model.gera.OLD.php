<?php
    function usuario($token){ 
        
        global $url_consulta_saldo;

        $url = $url_consulta_saldo;

        $data = []; 

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array('Authorization:'.$token.''));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $resposta = curl_exec($ch);
        curl_close($ch);             
        
        $respostaArray = json_decode($resposta, true);               
        
        return $respostaArray;
    }

    function produtoUsuarioDisponivel($token, $tipoProduto, $idCategoria, $ativoMostrarTelaPrincipal, $ativoTrocados, $textoPesquisa, $idParceiro, $ativoEventoNatal, $valorMin, $valorMax){
        $connection = abirConnection();   
        $params = array($token, $tipoProduto, $idCategoria, $ativoMostrarTelaPrincipal, $ativoTrocados, $textoPesquisa, $idParceiro, $ativoEventoNatal, $valorMin, $valorMax);
        
        $dados = executeSP("spProdutoUsuario_Disponivel", $params, $connection);                
        return $dados;
    }
   

    function categoria(){
        $connection = abirConnection();   
        $params = array();
        $dados = executeSP("spCategoria_Selecionar", $params, $connection);        
        return $dados;
    } 

    function parceiros($idParceiro){
        $connection = abirConnection();   
        $params = array($idParceiro);
        $dados = executeSP("spParceiros_Selecionar", $params, $connection);        
        return $dados;
    }

    function parceirosGrupo($idParceirosGrupo){
        $connection = abirConnection();   
        $params = array($idParceirosGrupo);
        $dados = executeSP("spParceirosGrupo_Selecionar", $params, $connection);        
        return $dados;
    }

    function parceirosAtivos($idParceiro, $idParceirosGrupo){
        $connection = abirConnection();   
        $params = array($idParceiro, $idParceirosGrupo);
        $dados = executeSP("spParceirosAtivos_Consultar", $params, $connection);        
        return $dados;
    }

    function carrinho($tipoAcao, $idCarrinho, $token, $id_usuario_pf){
        $connection = abirConnection();   
        $params = array($tipoAcao, $idCarrinho, $token, $id_usuario_pf);
        $dados = executeSP("spCarrinho", $params, $connection);        
        return $dados;
    }

    function carrinhoProduto($tipoAcao, $id_usuario_pf, $idCarrinho, $idProduto, $numeroQuantidade, $valorProduto, $token, $idParceiro){
        $connection = abirConnection();   
        $params = array($tipoAcao, $id_usuario_pf, $idCarrinho, $idProduto, $numeroQuantidade, $valorProduto, $token, $idParceiro);
        $dados = executeSP("spCarrinhoProduto", $params, $connection);        
        return $dados;
    }

  

    
    function tipoProduto($tipoProduto){        
        $nometipoProduto = '';

        $tipoProduto = strtoupper($tipoProduto);

        switch ($tipoProduto) {
            case 'P':
                $nometipoProduto = 'produto';
                break;
            case 'P':
                $nometipoProduto = 'servico';
                break;
            case 'VC':
                $nometipoProduto = 'servico';
                break;
            case 'G':
                $nometipoProduto = 'giftcard';
                break;
            case 'R':
                $nometipoProduto = 'recarga';
                break;
        }

        return $nometipoProduto;
    }

    
    function produto($idProduto){
        $connection = abirConnection();   
        $params = array($idProduto);
        $dados = executeSP("spProduto_Selecionar", $params, $connection);        
        return $dados;
    }
    
    function parceiroRetirada($idParceiro){
        $connection = abirConnection();   
        $params = array($idParceiro);
        $dados = executeSP("spParceiroRetirada_Selecionar", $params, $connection);        
        return $dados;
    }

    

    function produtoEstoqueDisponivel($token, $idProduto){
        $connection = abirConnection();   
        $params = array($token, $idProduto);        
        $dados = executeSP("spProdutoEstoque_Disponivel", $params, $connection);        
        return $dados;
    }
    
    function produtoImagem($tipoAcao, $idImagem, $idProduto, $nomeImagem){
        $connection = abirConnection();   
        $params = array($tipoAcao, $idImagem, $idProduto, $nomeImagem);
        $dados = executeSP("spProdutoImagem", $params, $connection);
        return $dados;
    }

    function periodoEntrega(){
        $connection = abirConnection();   
        $params = array();
        $dados = executeSP("spPeriodoEntrega_Consultar", $params, $connection);
        return $dados;
    }

    function estoqueDisponivelConsultar($tipoAcao, $idProduto, $nomeProduto, $percentualDesconto, $quantidadeDisponivel, $valorProduto, $idParceiro){
        $connection = abirConnection();   
        $params = array($tipoAcao, $idProduto, $nomeProduto, $percentualDesconto, $quantidadeDisponivel, $valorProduto, $idParceiro);
        $dados = executeSP("spEstoqueDisponivel_Consultar", $params, $connection);
        return $dados;
    }

    function compraProdutoConsultar($tipoAcao, $id_usuario_pf, $idCarrinho, $idCompra){
        $connection = abirConnection();   
        $params = array($tipoAcao, $id_usuario_pf, $idCarrinho, $idCompra);
        $dados = executeSP("spCompraProduto_Consultar", $params, $connection);
        return $dados;
    }
    
?>
