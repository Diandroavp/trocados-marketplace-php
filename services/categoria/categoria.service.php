<?php 

    header( 'Cache-Control: no-cache' );
	header( 'Content-type: application/xml; charset="utf-8"', true );

    include "../../funcoes/funcoes.php";    
    include "../../config/config.php";    
    include "../../model/index.php";
    
    $nomeFuncao = (isset($_REQUEST['nomeFuncao'])) ? $_REQUEST['nomeFuncao'] : null;
    $token = (isset($_REQUEST['token'])) ? $_REQUEST['token'] : null;
    
    if ($nomeFuncao == 'categoria') {
        
        // $dados = categoria();
        $dados = categoria($token);         

        $retorno = null;
        
        if ($dados){            
            $div = '<div class=" card-categoria">    
                        <div class="slick-container " >
                            <div class="slick" >';
            $divForm = '';

            foreach($dados as $item){
                $funcao = "'frmCategoria".$item['idCategoria']."'";
                $textoEvidencia = ($item['ativoEvidencia'] == 1) ? 'cor-trocados-ativo' : '';
                $div = $div.'<a href="#" class="slide " onclick="enviaFormularioSimples('.$funcao.')">
                                <div class="pad15 ">
                                <div class="text-center categoria-texto '.$textoEvidencia.'">'.limitarTexto($item['nomeCategoria'],12).'</div>
                                <!--<img class="text-center categoria-texto" src="'.$item['nomeImagem'].'">-->
                                </div>					                  
                            </a>                                         
                            
                            ';

                $divForm = $divForm.'<form role="form" id="frmCategoria'.$item['idCategoria'].'" name="frmCategoria'.$item['idCategoria'].'" action=""  method="post">                                    
                                        <input type="hidden" name="nomeCategoria"  value="'.$item['nomeCategoria'].'" />
                                        <input type="hidden" name="idCategoria" value="'.$item['idCategoria'].'" />
                                        <input type="hidden" name="_route"  value="categoria" />
                                    </form>';
            }
            
            $div = $div.'        </div>
                            </div>  
                        </div';

            $retorno[] = array( 
                'divCategoria' => $div, 
                'divFormCategoria' => $divForm, 
                'divScript' => '<script src="js/slide.touch.js"></script>',    
                );        
        } 

    } elseif ($nomeFuncao == 'categoria_valor') {
        
        // Montando tipo de Pesquisa
        $dados = array();
        $dados[] = array('tipoValor' =>  '1');
        $dados[] = array('tipoValor' =>  '2');
        $dados[] = array('tipoValor' =>  '3');
        $dados[] = array('tipoValor' =>  '4');
        //$dados[] = array('tipoValor' =>  '5');     

        $retorno = null;
        
        if ($dados){            
            $div = '<div class="">    
                        <div class="slick-container " >
                            <div class="slick-4" >';
            $divForm = '';

            foreach($dados as $item){
                $funcao = "'frmBuscaValor".$item['tipoValor']."'";                
                $div = $div.'<a href="#" class="slide slick-padding " onclick="enviaFormularioSimples('.$funcao.')">
                                <div class=" shadow  bg-white rounded " style="height:auto !important; " >
                                    <div class="product-grid8">
                                        <div class="product-image8 centralizarImagemDiv">                        
                                        <img class=" " style=" width: 100% " src="img/categoria-valor/cat-'.$item['tipoValor'].'new.png" />
                                        </div>
                                    </div>
                                </div>				                  
                            </a>                                         
                            
                            ';

                $divForm = $divForm.'<form role="form" id="frmBuscaValor'.$item['tipoValor'].'" name="frmBuscaValor'.$item['tipoValor'].'" action=""  method="post">                                    
                                        <input type="hidden" name="tipoValor"  value="'.$item['tipoValor'].'" />                                        
                                        <input type="hidden" name="_route"  value="buscar" />
                                    </form>';
            }
            
            $div = $div.'        </div>
                            </div>  
                        </div';

            $retorno[] = array( 
                'divCategoriaValor' => $div, 
                'divFormCategoriaValor' => $divForm, 
                'divScript' => '<script src="js/slide.touch-4.js"></script>',    
                );        
        } 

    } 

    echo (json_encode( $retorno ));
?>
