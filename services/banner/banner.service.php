<?php 

    header( 'Cache-Control: no-cache' );
	header( 'Content-type: application/xml; charset="utf-8"', true );

    include "../../funcoes/funcoes.php";    
    include "../../config/config.php";    
    include "../../model/index.php";
    
    $nomeFuncao = (isset($_REQUEST['nomeFuncao'])) ? $_REQUEST['nomeFuncao'] : null;
    $token = (isset($_REQUEST['token'])) ? $_REQUEST['token'] : null;
    
    if ($nomeFuncao == 'banner') {
        
        $dados = array();
      /*  $dados[] = array(
            'idBanner' =>  '10',
            'nomeImagem' =>  'banner-principal-14-maiores descontos-min.png',
            'idCategoria' => '8',
            'nomeCategoria' => 'Maiores descontos',
        );
        $dados[] = array(
            'idBanner' =>  '11',
            'nomeImagem' =>  'banner-principal-15-roupas-min.png',
            'idCategoria' => '12',
            'nomeCategoria' => 'Roupas',
        );
        $dados[] = array(
            'idBanner' =>  '12',
            'nomeImagem' =>  'banner-principal-16-maquiagens-min.png',	
            'idCategoria' => '35',
            'nomeCategoria' => 'Maquiagem',
        );
        $dados[] = array(
            'idBanner' =>  '13',
            'nomeImagem' =>  'banner-principal-17-geeks-min.png',
            'idCategoria' => '36',
            'nomeCategoria' => 'Geek',
        );
        $dados[] = array(
            'idBanner' =>  '14',
            'nomeImagem' =>  'banner-principal-18-acessorios-min.png',	
            'idCategoria' => '38',
            'nomeCategoria' => 'Acessórios',
        );  
*/
$dados[] = array(
    'idBanner' =>  '1',
    //'nomeImagem' =>  'banner-mercado_trocados.png',	
    'nomeImagem' =>  'banner-to-you.png',	    
    'idCategoria' => '',
    'nomeCategoria' => '',
); 

        $retorno = null;
        
        if ($dados){            
            $div = '<div class="">    
                        <div class="slick-container " >
                            <div class="slick-1" >';
            $divForm = '';

            foreach($dados as $item){
                $funcao = "'frmBanner".$item['idBanner']."'";                
                $div = $div.'<a href="#" class="slide slick-padding" onclick="enviaFormularioSimples('.$funcao.')" >
                                <div class="  " style="height:auto !important; " >
                                <div class="product-grid8">
                                    <div class="product-image8 centralizarImagemDiv">                        
                                        <img class=" " style=" width: 100%; border-radius:8px " src="img/banner/'.$item['nomeImagem'].'" />
                                    </div>
                                </div>
                                </div>
                            </a>                                             
                            
                            ';

                $divForm = $divForm.'<form role="form" id="frmBanner'.$item['idBanner'].'" name="frmBanner'.$item['idBanner'].'" action=""  method="post">       
                                        <input type="hidden" name="nomeCategoria"  value="'.$item['nomeCategoria'].'" />
                                        <input type="hidden" name="idCategoria" value="'.$item['idCategoria'].'" />                                     
                                        <input type="hidden" name="_route"  value="categoria" />
                                    </form>';
                
               // $divForm = '';
            }
            
            $div = $div.'        </div>
                            </div>  
                        </div';

            $retorno[] = array( 
                'divBanner' => $div, 
                'divFormBanner' => $divForm, 
                'divScript' => '<script src="js/slide.touch-1.js"></script>',    
                );        
        } 

        /*        
            <form role="form" id="frmBannerAlimento" name="frmBannerAlimento" action=""  method="post">
            
            <input type="hidden" name="nomeCategoria" id="nomeCategoria" value="Alimentos" />
            <input type="hidden" name="idCategoria" id="idCategoria" value="4" />
            <input type="hidden" name="_route" id="_route" value="categoria" />
            </form>

            <form role="form" id="frmBannerRelogio" name="frmBannerRelogio" action=""  method="post">
            
            <input type="hidden" name="nomeCategoria" id="nomeCategoria" value="Relógios" />
            <input type="hidden" name="idCategoria" id="idCategoria" value="2" />
            <input type="hidden" name="_route" id="_route" value="categoria" />
            </form>

            <form role="form" id="frmBannerBeleza" name="frmBannerBeleza" action=""  method="post">
            
            <input type="hidden" name="nomeCategoria" id="nomeCategoria" value="Beleza" />
            <input type="hidden" name="idCategoria" id="idCategoria" value="1" />
            <input type="hidden" name="_route" id="_route" value="categoria" />
            </form>
        */

    } 

    echo (json_encode( $retorno ));
?>
