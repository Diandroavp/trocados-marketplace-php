<?php 

    header( 'Cache-Control: no-cache' );
	header( 'Content-type: application/xml; charset="utf-8"', true );

    include "../../funcoes/funcoes.php";    
    include "../../config/config.php";    
    include "../../model/index.php";
    
    $token = (isset($_REQUEST['token'])) ? $_REQUEST['token'] : null;
    $nomeFuncao = (isset($_REQUEST['nomeFuncao'])) ? $_REQUEST['nomeFuncao'] : null;
    
    if ($nomeFuncao == 'endereco') {        
        $dados = usuarioEndereco($token, 's', null, null, null, null, null, null, null, null, null);

        $retorno = null;
        if ($dados){            
            $div = '';
            $divForm = '';
            
            foreach($dados as $item){     
                $endereco = $item['nomeEndereco'].", ".$item['numeroEndereco'].'';
                $funcao = "'frmEndereco'";   
                $div = $div.'
                                <a href="#" onclick="return enviaFormularioSimples('.$funcao.')" class="endereco"  >
                                    <div class="row mt-1 mb-1 ml-1 ml-1 ">                                        
                                        <div class="col-12 ">                            
                                                <span class="texteEndereco">'.$endereco.' </span>          
                                                <i class="fa fa-chevron-down faEndereco" aria-hidden="true"></i>                  
                                        </div>
                                        
                                    </div>
                                </a>                                
                            
                            ';

                $divForm = $divForm.'<form role="form" id="frmEndereco" name="frmEndereco" action="" method="post">    
                                        <input type="hidden" name="_route"  value="endereco" />
                                    </form>';
               
            }
            
            $div = $div.'';

            $retorno[] = array( 
                'div' => $div, 
                'divForm' => $divForm,                 
                );        
        } else {
            $div = '';
            $divForm = '';
            $funcao = "'frmEndereco'";      
            $div = $div.'<a href="#" onclick="return enviaFormularioSimples('.$funcao.', 0)" class="endereco"  >
                            <div class="row mt-1 mb-1 ml-1">
                                <div class="col-10 ">                            
                                        <i class="fa fa-home" aria-hidden="true"></i>                                        
                                        <span class="textoEndereco"> CLIQUE AQUI e informe seu endereço. </span>          
                                </div>
                                <div class="col-1 ">                            
                                    <i class="fa fa-chevron-down" aria-hidden="true"></i>                  
                                </div>
                            </div>
                        </a>                                
                        
                        ';

            $divForm = $divForm.'<form role="form" id="frmEndereco" name="frmEndereco" action="" method="post">    
                                    <input type="hidden" name="_route"  value="endereco" />
                                </form>';
               
           
            $div = $div.'';

            $retorno[] = array( 
                'div' => $div, 
                'divForm' => $divForm,                 
                );        
        } 

    } 

    echo (json_encode( $retorno ));
?>
