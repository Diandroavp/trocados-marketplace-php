<?php 

    header( 'Cache-Control: no-cache' );
	header( 'Content-type: application/xml; charset="utf-8"', true );

    include "../../funcoes/funcoes.php";    
    include "../../config/config.php";    
    include "../../model/index.php";    


    $nomeFuncao = (isset($_REQUEST['nomeFuncao'])) ? $_REQUEST['nomeFuncao'] : null;
    $token = (isset($_REQUEST['token'])) ? $_REQUEST['token'] : null;
    $idCarrinho = (isset($_REQUEST['idCarrinho'])) ? $_REQUEST['idCarrinho'] : null;
    $idProduto = (isset($_REQUEST['idProduto'])) ? $_REQUEST['idProduto'] : null;

    $retorno= null;

    if ($nomeFuncao == 'listar') {
        // BUSCANDO ID DO USUÁRIO
        $dadosUsuario = usuario($token);         
                
        if ((array_key_exists("code", $dadosUsuario)) && ($dadosUsuario['code'] == 200)) {        
            foreach ($dadosUsuario['data'] as $item){        
                $id_usuario_pf = $item['id_usuario_pf'];
            }
        }
        // BUSCANDO DADOS DO PRODUTO
        $dados = carrinhoProduto('s', $id_usuario_pf, null, null, null, null, null, null);     
        $dadosUsuarioEndereco = usuarioEndereco($token, 's', null, null, null, null, null, null, null, null, null);
        
        $retorno = null;
        $div = '';
        $divParceiro = null;
        $valorTotal = 0;
        $ativoEntrega = 0;
        $idCarrinho = null;
        $idParceiro = null;
        $nomeTipoProduto = tipoProduto('p');
        $valorFrete = 0;
        $ativoEntrega = 0;

        if ($dados){
            foreach($dados as $item){

                if ($dadosUsuarioEndereco) {
                    $dadosParceiros =  parceiros($item['idParceiro']);
    
                    if ($dadosParceiros) {
                        foreach ($dadosParceiros as $itemParceiro) {                                                
                            if ($itemParceiro['ativoEntrega']) {
                                if ($dadosUsuarioEndereco[0]['codigoIbge'] == $itemParceiro['codigoIbgeLocal']) {
                                    $valorFrete  = $itemParceiro['valorFreteLocal'];    
                                } else {
                                    $valorFrete  = $itemParceiro['valorFreteOutros'];    
                                }                            
                            }                        
                        }
                    }
                }
                


                $div = $div.'<div class="p-2" id="produto'.$item["idCarrinho"].$item["idProduto"].'">        
                                <div class="card card_desc_principal white shadow">            
                                    <div class="card-produto">
                                        <button type="button" class="close" onclick="return removerDoCarrinho('.$item["idCarrinho"].', '.$item["idProduto"].', \''.$item["nomeImagem"].'\', \''.$item["nomeProduto"].'\')"> 
                                            <span aria-hidden="true" class="" ><i class="fa fa-times" ></i></span><span class="sr-only" >Fechar</span>
                                        </button>						                                        
                                        <div class="">
                                            <label class="label-resumo">
                                                <div class="row ">
                                                    <div class="col-5 center">                                        
                                                        <img style="width:100%; min-width: 100px" src="'.$item["nomeImagem"].'"  onclick="enviaFormularioSimples(\'frmProduto'.$item['idProduto'].'\')"   />
                                                    </div>                
                                                    <div class="col-7 ">                                        
                                                        '.$item["nomeProduto"].'
                                                    </div>                
                                                </div> 
                                            </label>
                                            
                                            <div class="row ">
                                                <div class="col-5 ">                                        
                                                    <div class="btn-add-sub">
                                                        <div id="btn-menos'.$item["idProduto"].'"  onclick="return remQuantidade('.$item["idCarrinho"].', '.$item["idProduto"].')">
                                                            <svg viewBox="0 0 24 24" style="width:24px;height:24px" role="presentation">
                                                                <path d="M19,13H5V11H19V13Z" style="fill: rgb(153, 153, 153);"></path>
                                                            </svg>
                                                        </div>
                                                        <div id="numeroQuantidade'.$item["idProduto"].'">'.$item["numeroQuantidade"].'</div>
                                                        <div id="btn-mais'.$item["idProduto"].'"  onclick="return addQuantidade('.$item["idCarrinho"].', '.$item["idProduto"].')" >
                                                            <svg viewBox="0 0 24 24" style="width:24px;height:24px" role="presentation">
                                                                <path d="M19,13H13V19H11V13H5V11H11V5H13V11H19V13Z" style="fill:#9F1F90">
                                                                </path>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>                
                                                <div class="col-7 ">                                        
                                                    <div id="valorProduto'.$item["idProduto"].'" class="valorProdutoDesconto center" >
                                                    '.formatar_moeda($item["numeroQuantidade"] * $item["valorProduto"],2).'
                                                    </div>
                                                </div>                
                                            </div> 
                                            
                                        </div>                                            
                                    </div>
                                </div>    
                            </div>
                            
                            <form role="form" id="frmProduto'.$item['idProduto'].'" name="frmProduto'.$item['idProduto'].'" action="" method="post" >                                
                                <input type="hidden" name="idProduto"  value="'.$item['idProduto'].'" />    
                                <input type="hidden" name="_route"  value="'.tipoProduto($item['tipoProduto']).'" />                          
                            </form>
                            
                            ';
                $valorTotal      += $item["valorProduto"] * $item["numeroQuantidade"];
                $ativoEntrega    =  $item['ativoEntrega'];
                $idCarrinho      =  $item['idCarrinho'];
                $idParceiro      =  $item['idParceiro'];     
                if (!$divParceiro) {
                    $divParceiro = '
                    <label class="labelCategoria">Oferecido por</label>
                        <div class="p-2">                        
                        <div class="card_desc_principal white shadow  bg-white rounded">
                            <div class="card-produto">                                      
                                <label class="label-resumo">
                                <div class="row ">
                                    <div class="col-4 center">                                        
                                        <img style="width:40%; min-width: 50px" src="'.$item['nomeImagemParceiro'].'" />
                                    </div>                
                                    <div class="col-8 "> 
                                    '.$item['nomeParceiro'].'                                                                               
                                    </div>                
                                </div>             
                            </div>
                        </div>
                    </div>
                    ';           
                }
                
            }
            
            $retorno[] = array( 
                'divProduto' => $div,     
                'divParceiro' => $divParceiro,     
                'valorTotal' => formatar_moeda($valorTotal + $valorFrete,2),
                'valorTotalCarrinho' => $valorTotal + $valorFrete,

                'valorProdutos' => formatar_moeda($valorTotal,2),
                'valorTotalProdutos' => $valorTotal,

                'valorFrete' => formatar_moeda($valorFrete,2),
                'valorFreteCarrinho' => $valorFrete,

                'ativoRetorno' => 1,
                'ativoEntrega' => $ativoEntrega,
                'idCarrinho' => $idCarrinho,
                'idParceiro' => $idParceiro,
                'nomeTipoProduto' => $nomeTipoProduto,

                );
        } else {
            $retorno[] = array( 
                'divProduto' => null,   
                'divParceiro' => null,             
                'valorTotal' => 'R$ 0,00',
                'valorTotalCarrinho' => 0,

                'valorProdutos' => 'R$ 0,00',
                'valorTotalProdutos' => 0,

                'valorFrete' => 'R$ 0,00',
                'valorFreteCarrinho' => 0,

                'ativoRetorno' => 0,
                'ativoEntrega' => 0,
                'idCarrinho' => 0,
                'idParceiro' => 0,
                'nomeTipoProduto' => $nomeTipoProduto,
                );
        } 

    } elseif ($nomeFuncao == 'retirada') {
        // BUSCANDO ID DO USUÁRIO
        $dadosUsuario = usuario($token);         
                
        if ((array_key_exists("code", $dadosUsuario)) && ($dadosUsuario['code'] == 200)) {        
            foreach ($dadosUsuario['data'] as $item){        
                $id_usuario_pf = $item['id_usuario_pf'];
            }
        }
        // BUSCANDO DADOS DO PRODUTO
        $dados = carrinhoProduto('s', $id_usuario_pf, null, null, null, null, null, null); 
        
        $dadosRetirada = parceiroRetirada($dados[0]['idParceiro']);

        if ($dadosRetirada) {
            $timestamp = $_SERVER['REQUEST_TIME'];
            
            $dateNow = date('d/m/Y', $timestamp);
            //$newDate = date('d/m/Y', strtotime('+1 day', $timestamp));
            $newDate = date('d/m/Y', $timestamp);

            $dataEntrega = $newDate;
            //$horaEntregaInicial = '10:00';
            $horaEntregaInicial = date('h:m', strtotime($timestamp));

            foreach ($dadosRetirada as $item) {
                $retorno[] = array( 
                    'ativoEntrega' => 1,                
                    'textoEndereco' => $item['textoEndereco'],                
                    'nomeParceiro' => $item['nomeParceiro'],
                    //'dataEntrega' => _date_Y_m_d_to_br($item['dataEntrega']),
                    //'horaEntregaInicial' => _date_to_hour($item['horaEntregaInicial'], false),
                    //'horaEntregaFinal' => _date_to_hour($item['horaEntregaFinal'], false),    
                    'dataEntrega' => $dataEntrega,
                    'horaEntregaInicial' => $horaEntregaInicial,
                );
            }    
        } else {
            $retorno[] = array( 
                'ativoEntrega' => 0,   
            );
        }
        


    } elseif ($nomeFuncao == 'remover') {
        // DELETANDO UM PRODUTO
        $dados = carrinhoProduto('D', null, $idCarrinho, $idProduto, null, null, null, null);     
        
        $retorno = null;
        
        if ($dados){
            // BUSCANDO ID DO USUÁRIO
            $dadosUsuario = usuario($token);         
                    
            if ((array_key_exists("code", $dadosUsuario)) && ($dadosUsuario['code'] == 200)) {        
                foreach ($dadosUsuario['data'] as $item){        
                    $id_usuario_pf = $item['id_usuario_pf'];
                }
            }
            // BUSCANDO DADOS DO PRODUTO
            $dadosProduto = carrinhoProduto('s', $id_usuario_pf, null, null, null, null, null, null);     
        
            $valorTotal = 0;

            if ($dadosProduto){
                foreach($dadosProduto as $itemProduto){                
                    $valorTotal += $itemProduto["valorProduto"] * $itemProduto["numeroQuantidade"];
                }
            }


            foreach($dados as $item){                
                $retorno[] = array( 
                    'ativoRetorno' => $dados[0]['ativoRetorno'],
                    'textoRetorno' => $dados[0]['textoRetorno'],
                    'valorTotal' => formatar_moeda($valorTotal,2),
                    'valorTotalCarrinho' => $valorTotal,
                    );
                
            }
        } 
    } elseif ($nomeFuncao == 'addQuantidade') {
        // DELETANDO UM PRODUTO
        $dados = carrinhoProduto('AD', null, $idCarrinho, $idProduto, null, null, null, null);     
        
        $retorno = null;
        
        if ($dados){
            foreach($dados as $item){ 
                if ($dados[0]['ativoRetorno'] == 1) {
                    $retorno[] = array( 
                        'ativoRetorno' => $dados[0]['ativoRetorno'],
                        'textoRetorno' => $dados[0]['textoRetorno'],
                        'valorProduto' => formatar_moeda( $dados[0]['numeroQuantidade'] *  $dados[0]['valorProduto'],2),
                        'numeroQuantidade' => $dados[0]['numeroQuantidade'],
                        'valorTotal' => formatar_moeda($dados[0]['valorTotal'],2),
                        'valorTotalCarrinho' => $dados[0]['valorTotal'],
                        );
                } else {
                    $retorno[] = array( 
                        'ativoRetorno' => $dados[0]['ativoRetorno'],
                        'textoRetorno' => $dados[0]['textoRetorno'],                        
                        );
                }      
                
                
            }
        } 
    } elseif ($nomeFuncao == 'remQuantidade') {
        // DELETANDO UM PRODUTO
        $dados = carrinhoProduto('RM', null, $idCarrinho, $idProduto, null, null, null, null);     
        
        $retorno = null;
        
        if ($dados){           
            foreach($dados as $item){                
                $retorno[] = array( 
                    'ativoRetorno' => $dados[0]['ativoRetorno'],
                    'textoRetorno' => $dados[0]['textoRetorno'],
                    'valorProduto' => formatar_moeda( $dados[0]['numeroQuantidade'] *  $dados[0]['valorProduto'],2),
                    'numeroQuantidade' => $dados[0]['numeroQuantidade'],
                    'valorTotal' => formatar_moeda($dados[0]['valorTotal'],2),
                    'valorTotalCarrinho' => $dados[0]['valorTotal'],
                    );
                
            }
        } 
    }  


    echo (json_encode( $retorno ));
?>
