<?php 

    header( 'Cache-Control: no-cache' );
	header( 'Content-type: application/xml; charset="utf-8"', true );

    include "../../funcoes/funcoes.php";    
    include "../../config/config.php";    
    include "../../model/index.php";    

    $nomeFuncao = (isset($_REQUEST['nomeFuncao'])) ? $_REQUEST['nomeFuncao'] : null;
    $token = (isset($_REQUEST['token'])) ? $_REQUEST['token'] : null;

    $retorno[] = array(
                        'valorTotal' => formatar_moeda(0,2),
                        'ativoCarrinho' => 0,
                        );

    if ($nomeFuncao == 'carrinho') {
        $dadosUsuario = usuario($token);         
                
        if ((array_key_exists("code", $dadosUsuario)) && ($dadosUsuario['code'] == 200)) {        
            foreach ($dadosUsuario['data'] as $item){        
                $id_usuario_pf = $item['id_usuario_pf'];
            }
        }

        $dados = carrinho('s', null, null, $id_usuario_pf);        

        $retorno = null;
        
        if ($dados){
            foreach($dados as $item){
                $retorno[] = array( 
                                    'valorTotal' => formatar_moeda($item['valorTotal'],2),                                    
                                    'quantidadeTotal' => $item['quantidadeTotal'],                                    
                                    'ativoCarrinho' => ($item['valorTotal'] > 0 ? 1 : 0),
                                    'idParceiro' => $item['idParceiro'],
                                    );
            }
        } 

    } elseif ($nomeFuncao == 'esvaziar') {
        // BUSCANDO ID DO USUÁRIO
        $dadosUsuario = usuario($token);         
                
        if ((array_key_exists("code", $dadosUsuario)) && ($dadosUsuario['code'] == 200)) {        
            foreach ($dadosUsuario['data'] as $item){        
                $id_usuario_pf = $item['id_usuario_pf'];
            }
        }
        // BUSCANDO DADOS DO PRODUTO
        $dados = carrinho('D', null, null, $id_usuario_pf);   
        
        $retorno = null;
        
        if ($dados) {
            foreach ($dados as $item) {
                $retorno[] = array( 
                    'ativoRetorno' => $dados[0]['ativoRetorno'],
                    'textoRetorno' => $dados[0]['textoRetorno'],
                );
            }    
        } else {
            $retorno[] = array( 
                'ativoRetorno' => 0,   
                'textoRetorno' => 'Não conseguimos esvaziar seu carrinho, tente novamente mais tarde!',
            );
        }
    } 


    echo (json_encode( $retorno ));
?>
