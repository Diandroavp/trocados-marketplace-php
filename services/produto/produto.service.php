<?php 

    header( 'Cache-Control: no-cache' );
	header( 'Content-type: application/xml; charset="utf-8"', true );

    include "../../funcoes/funcoes.php";    
    include "../../config/config.php";    
    include "../../model/index.php";    


    $nomeFuncao = (isset($_REQUEST['nomeFuncao'])) ? $_REQUEST['nomeFuncao'] : null;
    $token = (isset($_REQUEST['token'])) ? $_REQUEST['token'] : null;
    $idCarrinho = (isset($_REQUEST['idCarrinho'])) ? $_REQUEST['idCarrinho'] : null;
    $idProduto = (isset($_REQUEST['idProduto'])) ? $_REQUEST['idProduto'] : null;
    $idParceiro = (isset($_REQUEST['idParceiro'])) ? $_REQUEST['idParceiro'] : null;

    $retorno= null;

    if ($nomeFuncao == 'addProduto') {
        $dadosUsuario = usuario($token);         
                
        if ((array_key_exists("code", $dadosUsuario)) && ($dadosUsuario['code'] == 200)) {        
            foreach ($dadosUsuario['data'] as $item){        
                $id_usuario_pf = $item['id_usuario_pf'];
            }
        }

        // DELETANDO UM PRODUTO
        $dados = carrinhoProduto('AP', $id_usuario_pf, $idCarrinho, $idProduto, null, null, $token, $idParceiro);     
        
        $retorno = null;
        
        if ($dados){
            foreach($dados as $item){  
                if ($dados[0]['ativoRetorno']){
                    $retorno[] = array( 
                        'ativoRetorno' => $dados[0]['ativoRetorno'],
                        'textoRetorno' => $dados[0]['textoRetorno'],
                        'valorProduto' => formatar_moeda( $dados[0]['numeroQuantidade'] *  $dados[0]['valorProduto'],2),
                        'numeroQuantidade' => $dados[0]['numeroQuantidade'],
                        'valorTotal' => formatar_moeda($dados[0]['valorTotal'],2),
                        'quantidadeTotal' => $item['quantidadeTotal'],  
                        );
                } else {
                    $retorno[] = array( 
                        'ativoRetorno' => $dados[0]['ativoRetorno'],
                        'textoRetorno' => $dados[0]['textoRetorno'],                        
                        );
                }             
                
                
            }
        } 
    } elseif ($nomeFuncao == 'getProduto') {
        $dadosUsuario = usuario($token);         
                
        if ((array_key_exists("code", $dadosUsuario)) && ($dadosUsuario['code'] == 200)) {        
            foreach ($dadosUsuario['data'] as $item){        
                $id_usuario_pf = $item['id_usuario_pf'];
            }
        }

        // DELETANDO UM PRODUTO
        $dados = carrinhoProduto('S', $id_usuario_pf, null, $idProduto, null, null, $token, null);     
        
        $retorno = null;
        
        if ($dados){
            foreach($dados as $item){   
                $valorTotal = $dados[0]['valorTotal'];                
                $quantidadeTotal = $dados[0]['quantidadeTotal'];

                if (($valorTotal == '') || ($quantidadeTotal == '')) {
                    $dadosCarrinho = carrinho('s', null, null, $id_usuario_pf);   

                    $valorTotal = $dadosCarrinho[0]['valorTotal'];
                    $quantidadeTotal = $dadosCarrinho[0]['quantidadeTotal'];
                }
                
                $retorno[] = array( 
                    'valorProduto' => formatar_moeda( $dados[0]['numeroQuantidade'] *  $dados[0]['valorProduto'],2),
                    'numeroQuantidade' => $dados[0]['numeroQuantidade'],
                    'valorTotal' => formatar_moeda($valorTotal,2),
                    'quantidadeTotal' => $quantidadeTotal, 
                    'idParceiro' => $item['idParceiro'],
                    'ativoRetorno' => 1,
                    );
                
            }
        } else {
            
            $dadosCarrinho = carrinho('s', null, null, $id_usuario_pf);   

            $valorTotal = $dadosCarrinho[0]['valorTotal'];
            
            
            $retorno[] = array( 
                'valorProduto' => 'R$ 0,00',
                'numeroQuantidade' => 0,
                'valorTotal' => formatar_moeda($valorTotal,2),
                'quantidadeTotal' => 0, 
                'ativoRetorno' => 0,
                );
        }
    }


    echo (json_encode( $retorno ));
?>
