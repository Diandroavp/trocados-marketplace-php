<div class=" <?=($ativo_uso_app) ? 'corpo-pos-cabecalho-app' : 'corpo-pos-cabecalho' ?>     " style="min-height: 650px;">
    <?php    
        $_route = (isset($_REQUEST['_route'])) ? $_REQUEST['_route'] : null;       
        
        include "src/carrinho/index.php";     

        switch($_route) {        
            case "produto": /*PRODUTO*/
                include "src/produto/index.php";            
                //
                break;
            case "servico": /*FLUXO DE COMPRA DE SERVICO*/
                include "src/servico/index.php";            
                //
                break;
            case "giftcard": /*FLUXO DE COMPRA DE GIFT CARD*/
                include "src/giftcard/index.php";            
                //
                break;
            case "recarga": /*FLUXO DE COMPRA DE RECARGAS*/
                include "src/recarga/index.php";            
                //
                break;
            case "categoria": /*CATEGORIA*/
                include "src/categoria/index.php";            
                //
                break;
            case "comprar": /*COMPRA*/
                include "src/compra/index.php";            
                //
                break;        
            case "buscar": /*BUSCAR*/
                include "src/busca/index.php";            
                //
                break;
            case "parceirosGrupo": /*Parceiros Grupo*/
                include "src/parceirosGrupo/index.php";            
                //
                break;                
            case "carrinho": /*Parceiros Grupo*/
                include "src/carrinhoProduto/index.php";            
                //
                break;  
            case "parceiros": /*Parceiros*/
                include "src/parceiros/index.php";            
                //
                break;    
            case "endereco": /*Endereco*/
                include "src/endereco/index.php";            
                //
                break;    
            default: /*PÁGINA PRINCIPAL DE MKPLACE*/                    
                    include "src/home/index.php";                     
                break;
        }    
    ?>
</div>