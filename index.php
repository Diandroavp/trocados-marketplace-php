<?php
    include("funcoes/funcoes.php");         
    include("config/config.php");    
?>
<!DOCTYPE html>
<html lang="en">
    <?php    
        include('head.php');
    ?>
    <?php
        // DEIXA O PROCESSAMENTO MAIS RAPIDO
        flush(); ?>
<script>
 
    $(document).ready(function() {        
        $("a").click(function(){
           // loaderTrocadosJs();
        });        
    });   

    function loaderTrocadosJs(){
    $("#overlay").css('display', 'block');

    var animation = bodymovin.loadAnimation({
            container: document.getElementById('lt'), 
                path: 'funcoes/loader.json', 
                renderer: 'svg', 
                loop: true, 
                autoplay: true, 
                name: 'Trocados', 
            })
            window.onload = function() {
                $("#overlay").css('display', 'none');
            };
    }    
</script>
    
  <body id="page-top" >        		
        <div id='overlay' style="display: none">
            <div id='card'>
                <div id='lt'></div>
            </div>
        </div> 
        <?php     
           // loaderTrocados();       
            include('titulo.php');         
            include('model/index.php');                  
            include('controller.php');      


            
   /*         
$indicesServer = array('PHP_SELF',
'argv',
'argc',
'GATEWAY_INTERFACE',
'SERVER_ADDR',
'SERVER_NAME',
'SERVER_SOFTWARE',
'SERVER_PROTOCOL',
'REQUEST_METHOD',
'REQUEST_TIME',
'REQUEST_TIME_FLOAT',
'QUERY_STRING',
'DOCUMENT_ROOT',
'HTTP_ACCEPT',
'HTTP_ACCEPT_CHARSET',
'HTTP_ACCEPT_ENCODING',
'HTTP_ACCEPT_LANGUAGE',
'HTTP_CONNECTION',
'HTTP_HOST',
'HTTP_REFERER',
'HTTP_USER_AGENT',
'HTTPS',
'REMOTE_ADDR',
'REMOTE_HOST',
'REMOTE_PORT',
'REMOTE_USER',
'REDIRECT_REMOTE_USER',
'SCRIPT_FILENAME',
'SERVER_ADMIN',
'SERVER_PORT',
'SERVER_SIGNATURE',
'PATH_TRANSLATED',
'SCRIPT_NAME',
'REQUEST_URI',
'PHP_AUTH_DIGEST',
'PHP_AUTH_USER',
'PHP_AUTH_PW',
'AUTH_TYPE',
'PATH_INFO',
'ORIG_PATH_INFO') ;

echo '<table cellpadding="10">' ;
foreach ($indicesServer as $arg) {
    if (isset($_SERVER[$arg])) {
        echo '<tr><td>'.$arg.'</td><td>'.$_SERVER[$arg].'</td></tr>' ;
    }
    else {
        echo '<tr><td>'.$arg.'</td><td>-</td></tr>' ;
    }
}
echo '</table>' ;

*/
                                        
              ?>
        
  </body>
  <script src="js/slide.touch.js"></script>
  <script src="js/funcoes-carrinho.js"></script>  
  <script src="js/MDB5/mdb.min.js"></script>   
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-KHWRBDH76T"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'G-KHWRBDH76T');
    </script>    
</html>

