<?php



function splashAvisoAguarde(){
	echo "

	
	<div class='modal' tabindex='-1' role='dialog' id='myModalAguarde'>
		<div id='loader'>				
			<div class='loader'></div>
		</div>	
		<div class='modal-footer'>
			<span id='close-modal-aguarde' data-dismiss='modal'></span>
		</div>					
	</div>	
	<span  id='open-modal-aguarde' data-toggle='modal' data-target='#myModalAguarde'></span>	
	
	<script>
			// sleep time expects milliseconds
			function sleep (time) {
			  return new Promise((resolve) => setTimeout(resolve, time));
			}
	
		
			$('#open-modal-aguarde').trigger('click');
			$(document).ready(function () {
			//$('#myModalAguarde').show();
			$(window).load(function () {
				// Quando a página estiver totalmente carregada, remove o id
				//$('#myModalAguarde').fadeOut('slow');
				// Usage!
				sleep(500).then(() => {
					// Do something after the sleep!
					$('#close-modal-aguarde').trigger('click');
				});				
			});
		});
		</script>

	";
}

function loaderTrocados(){
	echo " <script>		
			loaderTrocadosJs(); 	
			</script>";
}


function VerificaNavegadorSO() {
    $ip = $_SERVER['REMOTE_ADDR'];

    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version= "";

    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'Linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'Mac';
    }
    elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'Windows';
    }


    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
    {
        $bname = 'Internet Explorer';
        $ub = "MSIE";
    }
    elseif(preg_match('/Firefox/i',$u_agent))
    {
        $bname = 'Mozilla Firefox';
        $ub = "Firefox";
    }
    elseif(preg_match('/Chrome/i',$u_agent))
    {
        $bname = 'Google Chrome';
        $ub = "Chrome";
    }
    elseif(preg_match('/AppleWebKit/i',$u_agent))
    {
        $bname = 'AppleWebKit';
        $ub = "Opera";
    }
    elseif(preg_match('/Safari/i',$u_agent))
    {
        $bname = 'Apple Safari';
        $ub = "Safari";
    }

    elseif(preg_match('/Netscape/i',$u_agent))
    {
        $bname = 'Netscape';
        $ub = "Netscape";
    }

    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
    ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
    }


    $i = count($matches['browser']);
    if ($i != 1) {
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
            $version= $matches['version'][0];
        }
        else {
            $version= $matches['version'][1];
        }
    }
    else {
        $version= $matches['version'][0];
    }

    // check if we have a number
    if ($version==null || $version=="") {$version="?";}

    $Browser = array(
            'userAgent' => $u_agent,
            'name'      => $bname,
            'version'   => $version,
            'platform'  => $platform,
            'pattern'    => $pattern
    );

    $navegador = $Browser['name'] . " " . $Browser['version'];
    $so = $Browser['platform'];

    return $bname;/* Para finalizar coloquei aqui o meu insert para salvar na base de dados... Não fiz nada para mostrar em tela, pois só uso para fins de log do sistema  */
}


function showMessage($tipoMensagem = null, $tituloMensagem = null, $textMensagem = null){
	$tituloMensagem = $tituloMensagem;
	$textMensagem = $textMensagem;
	$retornar = "";
	
	switch($tipoMensagem) {
		case "E":
			$classMensagem =" alert alert-danger ";
			break;
		case "I": 
			$classMensagem =" alert alert-info ";
			break;
			
		case "A": 
			$classMensagem =" alert alert-warning ";
			break;
			
		case "S": 
			$classMensagem =" alert alert-success ";
			break;

	   
		default:
			$classMensagem =" alert alert-info ";
			break;
	}
	
	include "funcoes/include.modal.php"	;
    
     echo"<span  id='open-modal' data-toggle='modal' data-target='#modal'></span>";
        
     echo "<script> $('#open-modal').trigger('click'); </script>";
   
}


function enviaEmail($destinatario, $titulo, $conteudo, $conteudo_texto ){
	/*
	* EXEMPLO DE USO
	*enviaEmail("email@dominio.com.br", "Titulo do e-mail", "Corpo do e-mail em html", "Corpo do e-mail em texto plano");
	*/
	include "include.email.php";
	//
	if ($retorno) {
		erroDLG("I", "E-mail enviado com sucesso!");
	} else {
		erroDLG("E", "E-mail n&atilde;o enviado, tente novamente!");
	}
}

function enviaEmailRetorno($destinatario, $titulo, $conteudo, $conteudo_texto ){
	/*
	* EXEMPLO DE USO
	*enviaEmail("email@dominio.com.br", "Titulo do e-mail", "Corpo do e-mail em html", "Corpo do e-mail em texto plano");
	*/
	include "include.email.php";
	//
	if ($retorno) {
		$tipoRetorno = "1";
	} else {
		$tipoRetorno = "0";
	}
	
	return $tipoRetorno;
}


function splashAviso($tipo = false){
	// incluindo a tela de splash
	//include "include.splash.php";
	if ($tipo){
		echo "<script> document.getElementById('splash').style.display='block' </script>";
	} else {
		echo "<script>  document.getElementById('splash').style.display='none' </script>";
	}
}




function _geturlbase(){
	return _getsession("_SESSION_global_urlbase", "/");
}
function _getsession($_nomesession, $_defaultsession){
	$rootpath = realpath($_SERVER["DOCUMENT_ROOT"]);

	if (isset($_SESSION[$_nomesession])) {
		$_ss_value = $_SESSION[$_nomesession];
	} else {
		$_SESSION[$_nomesession] = $_defaultsession;
		$_ss_value = $_SESSION[$_nomesession];
	}
	return $_ss_value;
}


function formatarCPF($numero = '00000000000'){

	$parte_um     = substr($numero, 0, 3);
	$parte_dois   = substr($numero, 3, 3);
	$parte_tres   = substr($numero, 6, 3);
	$parte_quatro = substr($numero, 9, 2);

	return "$parte_um.$parte_dois.$parte_tres-$parte_quatro";

}




function _saveimagedir($idfileimage,$pathdirimagem){

   // Prepara a variÃ¡vel do arquivo
   $imagem = isset($_FILES[$idfileimage]) ? $_FILES[$idfileimage] : FALSE;

   // Tamanho mÃ¡ximo do arquivo (em bytes)
   $config["tamanho"] = 106883;
   // Largura mÃ¡xima (pixels)
   $config["largura"] = 350;
   // Altura mÃ¡xima (pixels)
   $config["altura"]  = 180;

   // Pega extensÃ£o do arquivo
   preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $imagem["name"], $ext);

   // Gera um nome Ãºnico para a imagem
   $imagem_nome = md5(uniqid(time())) . "." . $ext[1];

   // Caminho de onde a imagem ficarÃ¡
   $imagem_dir = $pathdirimagem.$imagem_nome;

   // Faz o upload da imagem
   move_uploaded_file($imagem["tmp_name"], $imagem_dir);

   return $imagem_nome;
}




function getDadosItensMenusSite() {
	global $_global_codigo_empresa, $connectionProtocolo;
	$query_consulta = "SELECT * FROM itenssite where idempresa = '".anti_sql_injection($_global_codigo_empresa)."'".
		" and ismenusite = '1' ".
		"order by ordem";

	$result1x = @mysql_query($query_consulta, $connectionProtocolo) or die ("Erro na conexÃ£o in query: $query " . mysql_error());
	$dados_item = array();
	while($dados=mysql_fetch_array($result1x)) {
		$dados_item[] = $dados;
	}
	return $dados_item;
}


function getDadosItemSite($_itemmenu) {
	global $_global_codigo_empresa, $connectionProtocolo;
	$query_consulta = "SELECT * FROM itenssite where itemmenu = '".anti_sql_injection($_itemmenu)."' and idempresa = '".anti_sql_injection($_global_codigo_empresa)."'".
		" order by id desc limit 1";
	$result1x = @mysql_query($query_consulta, $connectionProtocolo) or die ("Erro na conexÃ£o in query: $query " . mysql_error());
	$dados_item = array();
	while($dados=mysql_fetch_array($result1x)) {
		$dados_item[] = $dados;
	}
	return $dados_item[0];
}

function getDadosItemUnico($_itemmenu) {
	global $_global_codigo_empresa, $connectionProtocolo;
	$query_consulta = "SELECT * FROM itens where itemmenu = '".anti_sql_injection($_itemmenu)."' and idempresa = '".anti_sql_injection($_global_codigo_empresa)."'".
		" order by id desc limit 10";

	$result1x = @mysql_query($query_consulta, $connectionProtocolo) or die ("Erro na conexÃ£o in query: $query " . mysql_error());
	$dados_item = array();
	while($dados=mysql_fetch_array($result1x)) {
		$dados_item[] = $dados;
	}
	return $dados_item[0];
}

function getDadosItemDetalhes($iditem ) {
	global $_global_codigo_empresa, $connectionProtocolo;
	$query_consulta = "SELECT * FROM itens where id = '".anti_sql_injection($iditem)."' and idempresa = '".anti_sql_injection($_global_codigo_empresa)."'";

	$result1x = @mysql_query($query_consulta, $connectionProtocolo) or die ("Erro na conexÃ£o in query: $query " . mysql_error());
	$dados_item = array();
	while($dados=mysql_fetch_array($result1x)) {
		$dados_item[] = $dados;
	}
	return $dados_item[0];
}

function getDadosItensVarios($_itemmenu, $numeroregistros = 100) {
	global $_global_codigo_empresa, $connectionProtocolo;
	$query_consulta = "SELECT * FROM itens where itemmenu = '".anti_sql_injection($_itemmenu)."' and idempresa = '".anti_sql_injection($_global_codigo_empresa)."'".
	" order by ordem desc, id desc limit ".$numeroregistros;
	$result1x = @mysql_query($query_consulta, $connectionProtocolo) or die ("Erro na conexÃ£o in query: $query " . mysql_error());
	$dados_item = array();
	while($dados=mysql_fetch_array($result1x)) {
		$dados_item[] = $dados;
	}
	return $dados_item;
}

function getDadosItensGaleria($grupogaleria, $numeroregistros = 100) {
	global $_global_codigo_empresa, $connectionProtocolo;
	$query_consulta = "SELECT * FROM itens where categoria = 'galeria' and grupo ='".$grupogaleria."' and idempresa = '".$_global_codigo_empresa."'" ;
	$result1x = @mysql_query($query_consulta, $connectionProtocolo) or die ("Erro na conexÃ£o in query: $query " . mysql_error());
	$dados_item = array();
	while($dados=mysql_fetch_array($result1x)) {
		$dados_item[] = $dados;
	}
	return $dados_item;
}


function anti_sql_injection($str) {
    if (!is_numeric($str)) {
        $str = get_magic_quotes_gpc() ? stripslashes($str) : $str;
        $str = function_exists('mysql_real_escape_string') ? mysql_real_escape_string($str) : mysql_escape_string($str);
    }
    return $str;
}

function get_client_ip() {
		 $ipaddress = '';
		 if ($_SERVER['HTTP_CLIENT_IP'])
			 $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
		 else if($_SERVER['HTTP_X_FORWARDED_FOR'])
			 $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		 else if($_SERVER['HTTP_X_FORWARDED'])
			 $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		 else if($_SERVER['HTTP_FORWARDED_FOR'])
			 $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		 else if($_SERVER['HTTP_FORWARDED'])
			 $ipaddress = $_SERVER['HTTP_FORWARDED'];
		 else if($_SERVER['REMOTE_ADDR'])
			 $ipaddress = $_SERVER['REMOTE_ADDR'];
		 else
			 $ipaddress = 'UNKNOWN';

		 return $ipaddress;
	}

function exemploSELECT (){
     $sql = "SELECT * FROM pessoa";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
        // output data of each row
        while($row = mysqli_fetch_assoc($result)) {
            echo "id: " . $row["idPessoa"]. " - Name: " . $row["nomePessoa"]. " " . $row["senhaUsuario"]. "<br>";
        }
    } else {
        echo "0 results";
    }
    
    mysqli_close($conn);
}

function abirConnection(){
    // Create connection
	global $host, $user, $pass, $dbPortal, $tipoConexao;

	if ($tipoConexao == 1){		
		$conn = mysqli_connect(null, $user, $pass, $dbPortal, null, $host);		
		mysqli_set_charset($conn,"latin1");
	} else {
		$conn = mysqli_connect($host, $user, $pass, $dbPortal);   
		mysqli_set_charset($conn,"latin1");
	}

	return $conn;
}



function executeSP($namesp, $paramsIN = null, $connection ) {
        $tsql = "call _NAMESP_(_PARAMS_PLACES_);";
        $params_place = "";
        for($icont=0; $icont<=sizeof($paramsIN) -1; $icont++) {
            if (is_null($paramsIN[$icont])){            
                $params_place .= 'null';    
            } elseif (is_numeric($paramsIN[$icont])) {
                $params_place .= "$paramsIN[$icont]";
            } elseif (is_string($paramsIN[$icont])) {
                $valorUtf8 = utf8_decode($paramsIN[$icont]);
                $params_place .= "'$valorUtf8'";       
            } else {
                $params_place .= "'$paramsIN[$icont]'";       
            }
            
            if ($icont < sizeof($paramsIN) -1) {
                $params_place .= ", ";
            }
        }

        $tsql = str_replace("_NAMESP_", $namesp, $tsql);
        $tsql = str_replace("_PARAMS_PLACES_", $params_place, $tsql);
       
        
		//echo($tsql);

        $getitens = mysqli_query($connection, $tsql) or die("Erro ao consultar o banco de dados: ".mysqli_error());		

        $dados_item = array();
        
        if ($getitens){
            while( $row = mysqli_fetch_array( $getitens))
            {
                $dados_item[] = array_map('utf8_encode', $row); 
            }

            mysqli_free_result($getitens);             
        }
    
        return $dados_item;

    }



function executeSP_Dados($namesp, $paramsIN = null, $connection ) {

	$tsql = "{call _NAMESP_(_PARAMS_PLACES_)}";
	$params_place = "";
	$params_values = array();
	for($icont=1; $icont<=sizeof($paramsIN); $icont++) {
		$params_values[] = array($paramsIN[$icont-1], SQLSRV_PARAM_IN);
		$params_place .= "?";
		if ($icont < sizeof($paramsIN)) {
			$params_place .= ", ";
		}
	}

	$tsql = str_replace("_NAMESP_", $namesp, $tsql);
	$tsql = str_replace("_PARAMS_PLACES_", $params_place, $tsql);

	$getitens = mysql_query($connection, $tsql, $params_values);
	//echo $tsql."<br>";
	//print_r($params_values);

	return $getitens;
}

function permissionLoginSP($namesp, $login, $senha, $idBase) {
	global $connectionPortal;
	$dados = executeSP($namesp, array($login,$senha,$idBase), $connectionPortal);
	return ( sizeof($dados) > 0);
}


function getTipoPlataforma(){
	$isiPad = (bool) strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'ipad');
	$isiPhone = (bool) strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'iphone');
	$isAndroid = (bool) strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'android');
	$isWindows = (bool) strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'windows');
		  
	//Forma de aplicação
	if($isiPad || $isiPhone){
		$tipoPlataforma = 'IOS';
	} else if($isAndroid){
		$tipoPlataforma = 'ANDROID';
	} else if($isWindows){
		$tipoPlataforma = 'WINDOWS';
	} else {
		$tipoPlataforma = $_SERVER['HTTP_USER_AGENT'];            
	}

	return $tipoPlataforma;
}

//AUDITORIA
function inserirAuditoria($descricaoAuditoria, $tipoAuditoria, $textoAuditoria) {
	$connection = abirConnection();   	

	$params = array(	$descricaoAuditoria,								
						$tipoAuditoria,
						$textoAuditoria." - IP: ".get_client_ip()." - PLATAFORMA: ".$_SERVER['HTTP_USER_AGENT']
					);
	$dados = executeSP("spAuditoria_Registrar", $params, $connection);
	return ( sizeof($dados) > 0);
}

// Exportar CSV
function array_para_csv(array &$array)
{
   if (count($array) == 0) {
     return null;
   }
   ob_start();
   $df = fopen("php://output", 'w');
   fputcsv($df, array_keys(reset($array)));
   foreach ($array as $row) {
      fputcsv($df, $row);
   }
   fclose($df);
   return ob_get_clean();
}

function salvar_array_para_csv(array &$array, array &$cabecalho, $arquivo, $local, $download = false)
{
   if (count($array) == 0) {
     return null;
   }
   $df = fopen( $local.$arquivo, 'w');
   // preechendo linhas
    $array_novo = array();
    $textoLinha = "";
   
    // Colocando cabecalho
   //fputcsv($df, $cabecalho, ',');
    $tamanho = count($cabecalho);
    $contador = 1;
    foreach ($cabecalho as $col) {
        if ($contador < $tamanho)
            $textoLinha .= $col.',';  
        else 
            $textoLinha .= $col;
        //
        $contador++;
   }
    fwrite($df, $textoLinha); 

    $textoLinha = "\r\n";
    
   foreach ($array as $row) {
        $contador = 1;
		foreach ($cabecalho as $col){
            if ($contador < $tamanho){
                $textoLinha .= $row[$col].',';
            } else {
                $textoLinha .= $row[$col];
            } 
            //
            $contador++;
        }		  
		fwrite($df, $textoLinha); 
		//
        $textoLinha = "\r\n";
   }
        
   fclose($df);
   
   if ($download){
	   echo("<script> window.open('funcoes/include.arquivo.geral.php?file=".$arquivo."','_blank'); </script>");
	   
	   //cabecalho_download_csv($local.$arquivo);
   }
}

function cabecalho_download_csv($filename) {
    // desabilitar cache
	$new_name=$filename;
    $now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    // forçar download  
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    // disposição do texto / codificação
    header("Content-Disposition: attachment;filename={$new_name}");
    header("Content-Transfer-Encoding: binary");
	
}

function forcarLocate($local){
	echo("<form role='form' id='autoLoad' action='$local' method='post'></form>");
	echo("<script>document.getElementById('autoLoad').submit()</script>");
	////  header('location: index.php');
	
}

include "include.diretrizes.php"; 

?>
