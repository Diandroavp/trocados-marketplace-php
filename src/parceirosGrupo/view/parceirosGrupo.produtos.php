<div class="">    
<label class="labelCategoria">Produtos <b>Trocados</b></label>
    <div class="slick-container">
      <div class="slick-2">
	        <?php foreach($dadosProduto as $item) {?>            
            <a href="#" class="slide " onclick="enviaFormularioSimples('frmProduto<?=$item['idProduto']?>')" >
              <div class="slide-content" >
                <div class="product-grid8">
                    <div class="product-image8">                        
                            <img class="pic-1" src="<?=$item['nomeImagem']?>">
                            <img class="pic-2" src="<?=$item['nomeImagem']?>">
                        <?php if ($item['percentualDesconto'] && ($item['percentualDesconto'] > 0)) { ?>                    
                          <span class="product-discount-label">-<?=$item['percentualDesconto']?>%</span>
                        <?php }?>
                    </div>
                    <div class="product-content">
                        <span class="product-shipping" title="<?=$item['nomeProduto']?>" ><?=$item['nomeProduto']?></span>                          
                        <div class="price">R$ <?=$item['valorProduto']?>
                            <?php if ($item['percentualDesconto'] && ($item['percentualDesconto'] > 0)) { ?>
                              <span>R$ <?=$item['valorOriginal']?></span>
                            <?php }?>
                        </div>                        
                    </div>
                </div>
              </div>
            </a>                        
		      <?}?>   		  
      </div>
    </div>  
</div>
<?php foreach($dadosProduto as $item) {?>
	<form role="form" id="frmProduto<?=$item['idProduto']?>" name="frmProduto<?=$item['idProduto']?>" action="" method="post" >
		   
		<input type="hidden" name="idProduto"  value="<?=$item['idProduto']?>" />    
		<input type="hidden" name="_route"  value="<?=tipoProduto($item['tipoProduto'])?>" />                          
	</form>
<?}?>
