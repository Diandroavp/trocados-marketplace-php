<header class="cabecalho-top" style="width: 100%;  height: 80%">
    <div class=" cabecalho-logo-saldo">
        <div class="row" >
            <div class="col-6 center">                            
                <a href="#" onclick="enviaFormularioSimples('frmHome')">              
                    <img src="img/logo-branco.svg" alt="" style="width: 100%;  max-width: 250px"  >              
                </a>                                     
            </div>
            <div class="col-6">
                <!--<button href="#" type="button"  onclick="toggleFullScreen(document.body)" class="text-right text-success btn center"><i class='fas fa-expand-arrows-alt ' style='font-size:26px'></i></button>-->
            </div>
            
        </div>
        
    </div>
    <div class="container_centro">
        <div class="pesquisar box_centro">
            <input form="frmPesquisar"  type="text" name="url_direcionar" placeholder="Leia seu QR Code Aqui!" onkeydown="return enviaFormularioQR('frmPesquisar')" autocomplete="off" autofocus >
            <!--<a href="#" onclick="return enviaFormularioSimples('frmPesquisar')"  class="button-Serch"  ><i class="fa fa-qrcode fa-2x"></i></a> -->
            
        </div>
    </div>
    
</header>
<form role="form" id="frmHome" name="frmHome" action="?" method="post">    
</form>

<form class="" id="frmPesquisar" action="?">
</form>

<script>
    function enviaFormularioQR(form){                   
        var tecla=window.event.keyCode;
        var ctrl=window.event.ctrlKey;    //  Para Controle da Tecla CTRL

        if (ctrl && tecla==74){    //Evita teclar ctrl + j
            loaderTrocadosJs();
            event.keyCode=0;
            event.returnValue=false;           
            enviaFormularioSimples(form);               
             
        }
                
    }

    function toggleFullScreen(elem) {
        // ## The below if statement seems to work better ## if ((document.fullScreenElement && document.fullScreenElement !== null) || (document.msfullscreenElement && document.msfullscreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen)) {
        if ((document.fullScreenElement !== undefined && document.fullScreenElement === null) || (document.msFullscreenElement !== undefined && document.msFullscreenElement === null) || (document.mozFullScreen !== undefined && !document.mozFullScreen) || (document.webkitIsFullScreen !== undefined && !document.webkitIsFullScreen)) {
            if (elem.requestFullScreen) {
                elem.requestFullScreen();
            } else if (elem.mozRequestFullScreen) {
                elem.mozRequestFullScreen();
            } else if (elem.webkitRequestFullScreen) {
                elem.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
            } else if (elem.msRequestFullscreen) {
                elem.msRequestFullscreen();
            }
        } else {
            if (document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            }
        }
    }
</script>