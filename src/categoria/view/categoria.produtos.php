<?php if ($tipoApresentacaoGrid == 'rolagem') {?>

  <div class="">    
  <label class="labelCategoria">Produtos <b>Trocados</b></label>
    <div class="slick-container">
      <div class="slick-2">
	        <?php foreach($dadosProduto as $item) {?>            
            <div class="slide-content" >
            <a href="#" class="slide " onclick="enviaFormularioSimples('frmProduto<?=$item['idProduto']?>')" >
              
                <div class="product-grid8">
                    <div class="product-image8">                        
                            <img class="pic-1" src="<?=$item['nomeImagem']?>">
                            <img class="pic-2" src="<?=$item['nomeImagem']?>">
                        <?php if ($item['percentualDesconto'] && ($item['percentualDesconto'] > 0)) { ?>                    
                          <span class="product-discount-label">-<?=$item['percentualDesconto']?>%</span>
                        <?php }?>
                    </div>
                    <div class="product-content">
                        <span class="product-shipping" title="<?=$item['nomeProduto']?>" ><?=$item['nomeProduto']?></span>                          
                        <div class="price">R$ <?=formatar_moeda($item['valorProduto'],2)?>
                            <?php if ($item['percentualDesconto'] && ($item['percentualDesconto'] > 0)) { ?>
                              <span>R$ <?=formatar_moeda($item['valorOriginal'],2)?></span>
                            <?php }?>
                        </div>                        
                    </div>
                                          
                </div>
              
            </a>    
            <!--<button type="button" class="close verde text-right" onclick="return addProduto('<?=$item['idProduto']?>', '<?=$item['idParceiro']?>', '<?=$token?>', '<?=$item['nomeImagem']?>', '<?=$item['nomeProduto']?>')" > 
              <span aria-hidden="true" class="" ><i class="fa fa-cart-plus" > </i></span><span class="sr-only" >Adicionar</span>
            </button>-->
            <span class="titulo-parceiro" >Oferecido por: </span>   
            <span class="nome-parceiro" ><?=$item['nomeParceiro']?></span>                      
            <button 
                type="button" 
                class="btn btn-trocados center"                   
                style="width: 90%"
                onclick="return addProduto('<?=$item['idProduto']?>', '<?=$item['idParceiro']?>', '<?=$token?>', '<?=$item['nomeImagem']?>', '<?=$item['nomeProduto']?>')"
              > 
              <span aria-hidden="true" class="" > Adicionar <i class="fa fa-cart-plus" > </i></span>
            </button>
            </div>
		      <?}?>   		  
      </div>
    </div>  
</div>

<?php } else if ($tipoApresentacaoGrid == 'grade') {?>

  <label class="labelCategoria">Produtos <b>Trocados</b></label>  
  <div class="row" style="width: 100% !important; margin-left: 0px !important">  
    <?php foreach($dadosProduto as $item) {?>  
      <div class="col-6 col-md-4 col-sm-4 p-2 ">                  
      <div class="pad15 slide-content shadow  bg-white rounded">
          <a href="#" class=" slide" onclick="enviaFormularioSimples('frmProduto<?=$item['idProduto']?>')">              
            <div class="product-grid8 centralizarImagemDiv1">
              <img src="<?=$item['nomeImagem']?>" style="width: 100%"/>       							                    
              <div class="product-content">
                    <span class="product-shipping" title="<?=$item['nomeProduto']?>" ><?=$item['nomeProduto']?></span>                          
                    <div class="price"> <?=formatar_moeda($item['valorProduto'],2)?>
                        <?php if ($item['percentualDesconto'] && ($item['percentualDesconto'] > 0)) { ?>
                          <span><?=formatar_moeda($item['valorOriginal'],2)?></span>
                        <?php }?>
                    </div>                        
                </div>     
              </div>              					                  
          </a>
          <!--<button type="button" class="close verde text-right" onclick="return addProduto('<?=$item['idProduto']?>', '<?=$item['idParceiro']?>', '<?=$token?>', '<?=$item['nomeImagem']?>', '<?=$item['nomeProduto']?>')"> 
            <span aria-hidden="true" class="" ><i class="fa fa-cart-plus" > </i></span><span class="sr-only" >Adicionar</span>
          </button>-->
          <span class="titulo-parceiro" >Oferecido por: </span>   
          <span class="nome-parceiro" ><?=$item['nomeParceiro']?></span>  
          <button 
            type="button" 
            class="btn btn-trocados center"                   
            style="width: 90%"
            onclick="return addProduto('<?=$item['idProduto']?>', '<?=$item['idParceiro']?>', '<?=$token?>', '<?=$item['nomeImagem']?>', '<?=$item['nomeProduto']?>')"
          > 
            <span aria-hidden="true" class="" > Adicionar <i class="fa fa-cart-plus" > </i></span>
          </button>
        </div>
      </div>					                  
    <?}?>   		  

  </div>

<?php } ?>

<?php foreach($dadosProduto as $item) {?>
	<form role="form" id="frmProduto<?=$item['idProduto']?>" name="frmProduto<?=$item['idProduto']?>" action="" method="post" >
		   
		<input type="hidden" name="idProduto"  value="<?=$item['idProduto']?>" />    
		<input type="hidden" name="_route"  value="<?=tipoProduto($item['tipoProduto'])?>" />                          
	</form>
<?}?>
