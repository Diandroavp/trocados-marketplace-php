<div class="blocoConfirmar fixarRodape">   
        <div class="col-md-12">
            <?php // LIBERAR BOTAO DE COMPPRA
            if ($ativoLiberarCompra){?>
                <?php if ($textoRetornoDisponivel == '') { ?>     
                     
                    <div class="row">                                      
                        <div class="col-7">
                            <button id="btnConfirmar" type="button"  onclick="return adicionarProduto('<?=$token?>', '<?=$idProduto?>', '<?=$idParceiro?>')"  class="btn btn-lg btnAddCarrinho center" style="width: 100%" >                                                                       
                                <div class="row">
                                    <div class="col-2">                    
                                        <i class='fas fa-plus '> </i> 
                                    </div>
                                    <div class="col-2">                    
                                        <span id="numeroQuantidade">(0)</span>
                                    </div>
                                    <div class="col-8">                    
                                        <span id="valorProduto"> R$ 0,00</span>
                                    </div>
                                </div>                                
                            </button>
                        </div>
                        <div class="col-5">                    
                            <button id="btnConfirmar" type="button"  onclick="return enviaFormularioSimples('frmVoltar')"  class="btn  btnSair center" style="width: 100%" >
                                Comprar Mais
                            </button>                        
                        </div>                        
                        
                        <div class="col-12">          
                            <button type="button" id="btnCarrinho" class="btn btn-block btnConfirmacao" onclick="return enviaFormularioSimples('frmCarrinhoProduto')">
                                <div class="row">
                                    <div class="col-7">
                                        <span id="quantidadeCarrinhoProduto">(0)</span>                    
                                        <i class="fa fa-shopping-cart" > </i>                                             
                                            Ver carrinho
                                        </div>
                                    <div class="col-5">                    
                                        <span id="valorCarrinhoProduto"> R$ 0,00</span>
                                    </div>
                                </div>  
                            </button>
                        </div>                        
                    </div>

                    <?php } else { ?>
                    <div class="btnConfirmacao text-center">
                        <h3>Produto indisponível...</h3>
                    </div>
                <?php }?>
            
            <?php } else {?>
                <div class="btnConfirmacao text-center">
                    <h3>Chegamos ao fim...</h3>
                </div>
            <?php }?>
        </div>   
</div>

<form id="frmCarrinhoProduto" name="frmCarrinhoProduto" action="?" >            
    <input type="hidden" name="_route"  value="carrinho" />
    <input type="hidden" name="idParceiroCarrinho" id="idParceiroCarrinhoProduto" value="<?=$idParceiro?>"   />
</form>

<form class="pesquisar" id="frmVoltar" action="?" >                       
    
    <input type="hidden" name="_route"  value="" />
</form> 


<script>
    $(document).ready(function() {
        let token = '<?=$token?>';         
        let idProduto = '<?=$idProduto?>';                
        $("#btnCarrinho").prop('disabled', true);
        getCarrinhoProduto(token, idProduto);
    }); 
</script>