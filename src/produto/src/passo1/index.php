<?php include "produto.controller.php"?>

<!--CARD PRINCIPAL-->
<div class="col-12" >                
    <!-- SLIDE PRINCIPAL -->
    <?php include "produto.slide.php"?>       

    <!-- Valor do Produto -->
    <?php 
        if ($percentualDesconto > 0) {
            include "produto.valor.desconto.php";
        } else {
            include "produto.valor.php";
        }
        
    ?>
</div>
    
<!-- VALOR DO PRODUTO -->
<?php include "produto.descricao.php"?>

<!-- CONFIRMAR COMPRA-->
<?php include "produto.botao.confirmar.php"?>
<!-- Valor do Produto -->
<?php
    if ($ativoEntrega == 0) {
        // include "produto.alerta.retirada.php";
    }
?>

<!-- PARCEIRO -->
<?php include "produto.parceiro.php"?>        

