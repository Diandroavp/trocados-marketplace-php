<?php
    $idProduto = (isset($_REQUEST['idProduto'])) ? $_REQUEST['idProduto'] : null; 

    $dadosProduto = produto($idProduto);

    foreach ($dadosProduto as $item) {        
        $nomeProduto = $item['nomeProduto'];
        $nomeImagem = $item['nomeImagem'];       
        $textoDescricao = $item['textoDescricao'];
        $tipoProduto = $item['tipoProduto'];
        $ativoEntrega = $item['ativoEntrega'];
        $idParceiro = $item['idParceiro'];
    }

    $dadosProdutoEstoque = estoqueDisponivelConsultar('s', $idProduto, null, null, null, null, $idParceiro);

    foreach ($dadosProdutoEstoque as $item) {
        $valorProduto = number_format($item['valorProdutoLiquido'],2);
        $valorOriginal = number_format($item['valorProduto'],2);
        $valorProdutoFormatado = formatar_moeda($item['valorProdutoLiquido'],2);
        $valorOriginalFormatado = formatar_moeda($item['valorProduto'],2);
        $percentualDesconto = $item['percentualDesconto'];
    }

    $dadosParceiro = parceiros($idParceiro);

    foreach ($dadosParceiro as $item) {
        $nomeParceiro = $item['nomeParceiro'];        
        $nomeImagemParceiro = $item['nomeImagem'];        
    }

    $dadosProdutoImagem = produtoImagem('s', null, $idProduto, null, null);

    $dadosEstoque = produtoEstoqueDisponivel($token, $idProduto);
    $textoRetornoDisponivel = '';
    foreach ($dadosEstoque as $item) {
        $textoRetornoDisponivel = $item['textoRetornoDisponivel'];
    }

    $nomeProximoPasso = ($ativoEntrega == 1) ? 'passo2' : 'passo3_retirada';

?>