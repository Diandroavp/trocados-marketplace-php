<div class="p-3">
    <label class="titulo ">Oferecido por: </label>
    <!-- TEXTO DESCRITIVO-->
    <div class="card_desc_principal white shadow  bg-white rounded">
        <div class="card-produto">      
            <span class="sub-titulo-card">Parceiro</span>              
            <label class="label-resumo">
            <div class="row ">
                <div class="col-6 center">                                        
                    <img style="width:100%; min-width: 120px" src="<?=$nomeImagemParceiro?>" />
                </div>                
                <div class="col-6 ">                                        
                    <?=$nomeParceiro?>
                </div>                
            </div>             
        </div>
    </div>
</div>