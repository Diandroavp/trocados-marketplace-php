<div class="blocoValorProduto">
	<label class="labelCategoria"><?=$nomeProduto?></label>
	<hr>
	<div class="container-fluid">
		<div class="row ">
			<div class="col-6">
				<h1 class="percentualProduto text-center">
					<?=$percentualDesconto?>%
				</h1>
			</div>
			<div class="col-6">
				<div class="col-12 margin-left-right">
					<h3 class="valorProdutoOriginal">
						<?=$valorOriginalFormatado?>
					</h3>
				</div>
				<div class="col-12 margin-left-right">
					<h3 class="valorProdutoDesconto">
						<?=$valorProdutoFormatado?>						
					</h3>
				</div>
			</div>
		</div>
	</div>
</div>