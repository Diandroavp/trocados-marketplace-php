
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

<div class="container padding altura-min corpo-pos-cabecalho">
    <div class="jumbotron">
        <div class="text-center">
			<i class="fa fa-5x fa-frown-o" style="color:#d9534f;"></i>
		</div>
        <h1 class="text-center">404 Não Encontrado<p> </p><p><small class="text-center"> Parece que o usuário não foi encontrado</small></p></h1>
        <p class="text-center">Tente acessar outra opção ou tente novamente mais tarde.</p>
    </div>
</div>