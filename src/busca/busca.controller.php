<?php
    global $dadosUsuario, $token;
   
    // RETORNO DO FINAL DA COMPRA
    $idCategoria = (isset($_REQUEST['idCategoria'])) ? $_REQUEST['idCategoria'] : null; 
    $nomeCategoria = (isset($_REQUEST['nomeCategoria'])) ? $_REQUEST['nomeCategoria'] : null; 
    $textoBusca = (isset($_REQUEST['textoBusca'])) ? $_REQUEST['textoBusca'] : null; 
    $tipoValor = (isset($_REQUEST['tipoValor'])) ? $_REQUEST['tipoValor'] : null; 

    $valorMin = null;
    $valorMax = null;

    switch($tipoValor) {        
        case "1": 
            $valorMin = 0;
            $valorMax = 4.99;
            $textoBusca = null;
            $textoTitulo = 'Abaixo de R$ 4.99';
            //
            break;
        case "2": 
            $valorMin = 5;
            $valorMax = 9.99;
            $textoBusca = null;
            $textoTitulo = 'Entre R$ 5.00 e R$ 9.99';
            //
            break;
        case "3": 
            $valorMin = 10;
            $valorMax = 19.99;
            $textoBusca = null;
            $textoTitulo = 'Entre R$ 10.00 e R$ 19.99';
            //
            break;
        /* case "4": 
            $valorMin = 20;
            $valorMax = 29.99;
            $textoBusca = null;
            $textoTitulo = 'Entre R$ 20.00 e R$ 29.99';
            //
            break; */
        case "4": 
            $valorMin = 20;
            $valorMax = 300;            
            $textoBusca = null;
            $textoTitulo = 'Acima de R$ 20.00';
            //
            break;
            
        default: 
            $valorMin = null;
            $valorMax = null;
            $textoTitulo = $textoBusca;
            break;
    }

    $dadosParceirosAtivos = parceirosAtivos($token, $idParceiroGlobal, null, null, $ativo_trocados);
    
?>
