

<div class="col-12">        
    <label class="labelCategoria">Produtos</label>
    <div  id="carrinhoProduto" style=" padding-bottom: 60px">
        <div id='overlay_produto' class="overlay " style="display: none">            
            <div id='card'>
                <div id='lt_produto'></div>
            </div>            
        </div> 
    </div>  

    
    <div  id="divParceiro"></div>  

    <?php include "produtos.controller.php";?>

    
    <label class="labelCategoria">Valores da Compra</label>
    <div class=" p-2">
        <div class="card card_desc_principal white">            
            <div class="card-produto">      
                <div class="row ">
                    <div class=" col-6">                        
                            <label class="label-resumo">Valor Produtos: </label>                        
                    </div>
                    <div class=" col-6">
                        <strong>
                                <div class="valorProdutoDesconto" id="valorProdutos" >
                                    R$ 00,00
                                </div> 
                                <input type="hidden" id="valorTotalProdutos" />                                  
                        </strong>       
                    </div>
                </div> 

                <div class="row " id="divFrete">
                    <div class=" col-6">                        
                            <label class="label-resumo">Valor Frete: </label>                        
                    </div>
                    <div class=" col-6">
                        <strong>
                                <div class="valorProdutoDesconto" id="valorFrete" >
                                    R$ 00,00
                                </div> 
                                <input type="hidden" id="valorFreteCarrinho" />                                  
                        </strong>       
                    </div>
                </div> 

                <div class="row ">
                    <div class=" col-6">                        
                            <label class="label-resumo">Valor Total: </label>                        
                    </div>
                    <div class=" col-6">
                        <strong>
                                <div class="valorProdutoDesconto" id="valorTotal" >
                                    R$ 00,00
                                </div> 
                                <input type="hidden" id="valorTotalCarrinho" />                                  
                        </strong>       
                    </div>
                </div> 
            </div>
        </div>    
    </div>   

    <div class="p-2" id="semProduto" style="display: none">
        <div class="card card_desc_principal white" >            
            <div class="card-produto">      
                <div class="row ">
                    <div class=" col-12">
                        <center>
                                Você não possui produto no carrinho... 
                        </center>       
                    </div>
                </div> 
            </div>
        </div>
    </div>   
</div>



<script src="src/carrinhoProduto/js/carrinhoProduto.js"></script>
<script>   
    var animation2 = bodymovin.loadAnimation({
            container: document.getElementById('lt_produto'), 
            path: './funcoes/loader.json', 
            renderer: 'svg', 
            loop: true, 
            autoplay: true, 
            name: 'Trocados', 
        })

   $(document).ready(function() {
        let token = '<?=$token?>';    
        
        $("#overlay_produto").css('display', 'block');       
        $("#btnConfirmar").prop('disabled', true);

        listarProdutos(token);
    }); 

    // REMOVER O PRODUTO DO CARRINHO
    function removerDoCarrinho(idCarrinho, idProduto, nomeImagem, nomeProduto){
        let token = '<?=$token?>';    
        Swal.fire({
                    title: 'Remover do carrinho',
                    text: nomeProduto,                                        
                    showCancelButton: true,
                    showConfirmButton: true,
                    confirmButtonText: 'Look up',
                    imageUrl: nomeImagem,
                    imageWidth: 200,
                    imageHeight: 200,
                    showCancelButton: true,
                    confirmButtonColor: 'red',
                    cancelButtonColor: '#ccc',
                    confirmButtonText: 'Remover',
                    cancelButtonText: 'Cancelar',
                    preConfirm: () => {        
                        removerProdutos(token, idCarrinho, idProduto);
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                    }).then((result) => {
                    
                    })
    }

    function addQuantidade(idCarrinho, idProduto){
        let token = '<?=$token?>';              
        adicionarQuantidade(token, idCarrinho, idProduto);
       
    }

    function remQuantidade(idCarrinho, idProduto){
        let token = '<?=$token?>';              
        removerQuantidade(token, idCarrinho, idProduto);
       
    }
</script>
