<?php 
    $idParceiroCarrinho = (isset($_REQUEST['idParceiroCarrinho'])) ? $_REQUEST['idParceiroCarrinho'] : null;

    $ativoRetirada = 0;
    $ativoEntrega  = 0;  

    $dadosParceiros =  parceiros($idParceiroCarrinho);
    
    if ($dadosParceiros) {
        foreach ($dadosParceiros as $item) {
            $ativoRetirada = $item['ativoRetirada'];
            $ativoEntrega  = $item['ativoEntrega'];    
            $diasEntregaLocal  = $item['diasEntregaLocal'];    
            $diasEntregaOutros  = $item['diasEntregaOutros'];    
            $codigoIbgeLocal  = $item['codigoIbgeLocal'];    
        }
    }



    if ($ativoEntrega) {
        include "src/carrinhoProduto/src/entrega/index.php";
    } else if ($ativoRetirada) {
        include "src/carrinhoProduto/src/retirada/index.php";
    }
?>


<input type="hidden" form="frmCompraProduto" name="ativoRetirada" id="ativoRetirada" value="<?=$ativoRetirada?>" />
<input type="hidden" form="frmCompraProduto" name="ativoEntrega" id="ativoEntrega" value="<?=$ativoEntrega?>" />