
<form role="form" id="frmCompraProduto" name="frmCompraProduto" action="" method="post" >          
    <input type="hidden" name="idCarrinho" id="idCarrinho_carrinho" value="" />
    <input type="hidden" name="idParceiro" id="idParceiro_carrinho" value="" />
    <!--ROTAS-->
    <input type="hidden" name="_route" id="_route_carrinho" value="" />
    <input type="hidden" name="_route_origem" id="_route_origem_carrinho" value="<?=$_REQUEST['_route']?>" />
    <input type="hidden" name="_subroute" id="_subroute_carrinho" value="" />
</form>

<!-- CONFIRMAR COMPRA-->
<div class="blocoConfirmar fixarRodape">
    <div class="row">
        <div class="col-7" >            
            <button id="btnConfirmar" type="button" class="btn btnConfirmacao center"  style="width: 100%">
                Finalizar
                <span class="" id="valorTotalBotao" >
                    R$ 00,00
                </span>
            </button>
        </div>
        <div class="col-5">                    
            <button type="button"  onclick="return enviaFormularioSimples('frmVoltar')"  class="btn btnSair center" style="width: 100%" >
                Comprar Mais
            </button>                        
        </div>         
        <div class="col-12 pt-2" >            
            <button id="btnEsvaziarCarrinho" type="button" class="btn btnEsvaziar center" onclick="return esvaziarCarrinho('<?=$token?>')" style="width: 100%">
                Esvaziar Carrinho
            </button>
        </div>        
    </div>
</div>  

<form class="pesquisar" id="frmVoltar" action="?">                           
    <input type="hidden" name="_route" id="_route" value="" />
</form> 


<script>
    function getTextoRetiradaOutros(valorTipoRetirada){

        var nomePessoaRetirada = document.getElementById('nomePessoaRetirada').value;
        var tipoDocumentoRetirada = document.getElementById('tipoDocumentoRetirada').value;
        var numeroDocumentoRetirada = document.getElementById('numeroDocumentoRetirada').value;        

        if (valorTipoRetirada == 2){
            var     texto = '<div>';
            texto = texto + '<h5> Valide os dados da pessoa que irá retirar o produto!</h5>';
            texto = texto + '<div class="text-left"><b>Nome: </b></div>';
            texto = texto + '<div class="text-left">' + nomePessoaRetirada + '</div><br><br>';
            texto = texto + '<div class="text-left"><b>' + tipoDocumentoRetirada + ':  </b></div>';
            texto = texto + '<div class="text-left">' + numeroDocumentoRetirada + '</div>';
            texto = texto + '<br>';
            texto = texto + '</div>';
        } else {
            var     texto = '';
        }        

        return texto;
    }

    function getTextoEntrega(textoEnderecoEntrega){
        var     texto = '<div>';
        texto = texto + '<h5> Valide o endereço para onde a compra será enviada!</h5>';
        texto = texto + '<br>';
        texto = texto + '<div class="text-left">' + textoEnderecoEntrega + '</div><br><br>';
        texto = texto + '<br>';
        texto = texto + '</div>';
        
        return texto;
    }

    var botao = document.querySelector('#btnConfirmar');
    

    botao.addEventListener('click', function (event) {
        
        var ativoRetirada = document.getElementById('ativoRetirada').value;
        var ativoEntrega = document.getElementById('ativoEntrega').value;

        
        if (ativoEntrega == '1' ){
            // VALORES DE ENTREGA
            var nomeEnderecoEntrega = document.getElementById("nomeEnderecoEntrega");
            var numeroEnderecoEntrega = document.getElementById("numeroEnderecoEntrega");
            var nomeBairroEntrega = document.getElementById("nomeBairroEntrega");
            var numeroCepEntrega = document.getElementById("numeroCepEntrega");
            var codigoIbgeEntrega = document.getElementById("codigoIbgeEntrega");

            
            if ((nomeEnderecoEntrega.value == '') || (numeroEnderecoEntrega.value == '') || (numeroEnderecoEntrega.value == '000') || (nomeBairroEntrega.value == '') || (numeroCepEntrega.value == '') || (codigoIbgeEntrega.value == '') || (codigoIbgeEntrega.value == '000')) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    confirmButtonColor: '#33CB00',
                    text: 'Parece que tem alguma informação faltando no seu endereço! Clique sobre a opção de Endereço para alterar seus dados!'                
                })   
                return false;
            }

            var textoEnderecoEntrega = document.getElementById('textoEndereco').innerHTML;;
        } else if (ativoRetirada == '1' ) {
            var tipoAutorRetirada = document.getElementsByName("tipoAutorRetirada");
            var nomePessoaRetirada = document.getElementById("nomePessoaRetirada");
            var numeroDocumentoRetirada = document.getElementById("numeroDocumentoRetirada");

            var valorTipoRetirada = 1;

            for (var i = 0; i < tipoAutorRetirada.length; i++) {
                if (tipoAutorRetirada[i].checked) {
                    valorTipoRetirada = tipoAutorRetirada[i].value;
                }            
            }


            if ((valorTipoRetirada == 2) && ((nomePessoaRetirada.value == '') || (numeroDocumentoRetirada.value == ''))) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    confirmButtonColor: '#33CB00',
                    text: 'Você esqueceu de preencher os dados de quem irá retirar o produto por você!'                
                })   
                return false;
            }
        }
     


        var valorSaldo = <?=$valorSaldo?>;
        var valorTotalCarrinho = $("#valorTotalCarrinho").val();            
        var usoApp = <?=$ativo_uso_app?>;            

        if (valorSaldo >= valorTotalCarrinho ){
            if (usoApp == 1) {                
                Swal.fire({
                    title: 'Confirma a compra?',
                    // text: "Você não poderá reverter isso!",
                    input: 'password',
                    inputPlaceholder: 'Informe sua senha!',
                    html:  ativoEntrega == '1' ? getTextoEntrega(textoEnderecoEntrega) : ( ativoRetirada == '1' ? getTextoRetiradaOutros(valorTipoRetirada) : ''),
                    inputAttributes: {
                        //maxlength: 4,
                        autocapitalize: 'off'
                    },
                    showCancelButton: true,
                    showConfirmButton: true,                    
                    confirmButtonColor: '#33CB00',
                    cancelButtonColor: '#ccc',
                    confirmButtonText: 'Sim, Comprar!',
                    cancelButtonText: 'Cancelar',
                    preConfirm: (tag_usuario) => {                        
                        if (tag_usuario != '') {

                            loaderTrocadosJs();

                            let formulario = document.getElementById('frmCompraProduto');
                            
                            let elemento = document.createElement("input");
                            elemento.setAttribute('type', 'hidden');
                            elemento.setAttribute('name', 'tag_usuario');
                            elemento.setAttribute('value', tag_usuario);

                            formulario.appendChild(elemento);
                               
                            enviaFormularioSimples('frmCompraProduto');        
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                confirmButtonColor: '#33CB00',
                                text: 'Parece que você inseriu a sua senha!!'                
                            })            
                            return false; 
                        }                        
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                    }).then((result) => {
                    
                    })
            } else {
                Swal.fire({
                    title: 'Aproxime sua chave no leitor para confirmar a compra!',
                    text: "Você não poderá reverter isso!",
                    input: 'text',
                    //inputLabel: 'Insira sua chave para validar',
                    inputPlaceholder: 'Sua chave aqui!!',
                    html:  ativoEntrega == '1' ? getTextoEntrega(textoEnderecoEntrega) : ( ativoRetirada == '1' ? getTextoRetiradaOutros(valorTipoRetirada) : ''),
                    inputAttributes: {
                        autocapitalize: 'off'
                    },
                    showCancelButton: true,
                    showConfirmButton: false,                    
                    imageUrl: 'https://i.ibb.co/hffyVgx/qr-code-scan-hand.jpg',
                    imageWidth: 200,
                    imageHeight: 200,                    
                    confirmButtonColor: '#33CB00',
                    cancelButtonColor: '#ccc',
                    confirmButtonText: 'Sim, Comprar!',
                    cancelButtonText: 'Cancelar',
                    preConfirm: (token_validacao) => {                        
                        if (token_validacao != '') {
                            loaderTrocadosJs();

                            let formulario = document.getElementById('frmCompraProduto');
                            
                            let elemento = document.createElement("input");
                            elemento.setAttribute('type', 'hidden');
                            elemento.setAttribute('name', 'token_validacao');
                            elemento.setAttribute('value', token_validacao);

                            formulario.appendChild(elemento);
                               
                            enviaFormularioSimples('frmCompraProduto');              
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                confirmButtonColor: '#33CB00',
                                text: 'Parece que você inseriu a sua chave!!'                
                            })            
                            return false; 
                        }
                        
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                    }).then((result) => {
                    
                    })
            }
            
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                confirmButtonColor: '#33CB00',
                text: 'Você não possui saldo suficiente!!'                
            })            
            return false;  
        }
    });

    
</script>