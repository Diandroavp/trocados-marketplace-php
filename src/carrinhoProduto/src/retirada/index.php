<div id="divRetirada" style="display: none">
    <!--CARD PRINCIPAL-->
    <div class="col-12">        
        <label class="labelCategoria">Instruções de Retirada</label>

        <!-- OPÇÃO DE RETIRADA -->
        <?php include "retirada.texto.retirada.php"; ?>

        <!-- OPÇÃO DE RETIRADA DATA HORA -->
        <?php include "retirada.texto.retirada.datahora.php"; ?>

        <!-- OPÇÃO DE RETIRADA AUTOR -->
        <?php include "retirada.texto.retirada.autor.php"?>       
    </div>  
</div>
<script>

    $(document).ready(function() {
        let token = '<?=$token?>';    
        
        montarRetirada(token);
    }); 

    function mostrarBlocoRetiradaPorOutros(tipoRetirada){
        var blocoRetirada = document.getElementById('blocoRetiradaOutros');

        if (tipoRetirada == 1) {
            blocoRetirada.style.display = 'none';
        } else {
            blocoRetirada.style.display = 'block';
        }
    }
    
</script>
