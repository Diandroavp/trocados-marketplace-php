<div class="p-3" >
	<div class="card p-2">
		<label class="labelCategoria">Quem irá <b>retirar?</b></label>
		<div class="row ">
			<div class="col-12 ">
				<div class="input-group">
					<div class="input-group-prepend">
						<label class="labelCategoria label-resumo" style=" font-size:14px;"><input type="radio" form="frmCompraProduto" name="tipoAutorRetirada"
								value="1" checked onclick="mostrarBlocoRetiradaPorOutros(this.value)"> Você</label>
					</div>
				</div>
			</div>
		</div>

		<div class="row ">
			<div class="col-12 ">
				<div class="input-group ">
					<div class="input-group-prepend">
						<label class="labelCategoria label-resumo" style=" font-size:14px;"><input type="radio" form="frmCompraProduto" name="tipoAutorRetirada"
								value="2" onclick="mostrarBlocoRetiradaPorOutros(this.value)"> Uma outra pessoa</label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="p-3" id="blocoRetiradaOutros" style="display: none">
	<div class="card ">
		<div class="card-body padding">
			<div class="row">
				<div class="col-12">
					<div class="form-group">
						<label class="" for="nomePessoaRetirada">
							Nome Completo
						</label>
			 			<input type="text" form="frmCompraProduto" class="form-control" id="nomePessoaRetirada"
							name="nomePessoaRetirada" />
					</div>
				</div>
				<div class="col-12">
					<div class="form-group">
						<label class="" for="tipoDocumentoRetirada">
							Tipo do Documento
						</label>
						<select form="frmCompraProduto" class="custom-select form-control" id="tipoDocumentoRetirada"
							name="tipoDocumentoRetirada">
							<option value="RG">RG</option>
							<option value="CNH">CNH</option>
						</select>
					</div>
				</div>
				<div class="col-12">
					<div class="form-group">
						<label class="" for="numeroDocumentoRetirada">
							Número do Documento
						</label>
						<input type="text" form="frmCompraProduto" class="form-control" id="numeroDocumentoRetirada"
							name="numeroDocumentoRetirada" />
					</div>
				</div>
			</div>
		</div>
	</div>
</div>