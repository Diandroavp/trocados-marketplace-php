<div class="p-3">    
    <!-- TEXTO DESCRITIVO-->   
    <div class="card card_desc_principal white">
        <div class="card-produto">         
        <label class="titulo ">Data/Horário </label>
            <p class=" ">
                <p>
                    Produto disponível para retirada a partir da data abaixo.
                </p>
                <p class="label-resumo">
                    Dia: <span id="dataEntrega"></span>
                    <br>
                    Hora: <span id="horaEntregaInicial"></span>
                </p>                        
            </p>
        </div>
    </div>
</div>
