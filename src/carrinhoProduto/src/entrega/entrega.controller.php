<?php    
    // ENTREGA    
    $tipoAcao = (isset($_REQUEST['tipoAcao'])) ? $_REQUEST['tipoAcao'] : 'S';
    $idEndereco = (isset($_REQUEST['idEndereco'])) ? $_REQUEST['idEndereco'] : null;

    $nomeEndereco = (isset($_REQUEST['nomeEndereco'])) ? $_REQUEST['nomeEndereco'] : null;
    $numeroEndereco = (isset($_REQUEST['numeroEndereco'])) ? $_REQUEST['numeroEndereco'] : null;
    $nomeBairro = (isset($_REQUEST['nomeBairro'])) ? $_REQUEST['nomeBairro'] : null;
    $nomeComplemento = (isset($_REQUEST['nomeComplemento'])) ? $_REQUEST['nomeComplemento'] : '-';
    $numeroCep = (isset($_REQUEST['numeroCep'])) ? $_REQUEST['numeroCep'] : null;    
    $numeroCep = str_replace(".", "", $numeroCep);
    $numeroCep = str_replace("-", "", $numeroCep);
    $nomeCidade = (isset($_REQUEST['nomeCidade'])) ? $_REQUEST['nomeCidade'] : null;
    $nomeUF = (isset($_REQUEST['nomeUF'])) ? $_REQUEST['nomeUF'] : null;
    $codigoIbge = (isset($_REQUEST['codigoIbge'])) ? $_REQUEST['codigoIbge'] : null;
   
 
    $dadosEndereco = usuarioEndereco($token, $tipoAcao, null, null, null, null, null, null, null, null, null);    

    if ($dadosEndereco) {
        foreach ($dadosEndereco as $item) {
            $idEndereco = $item['idEndereco'];
            $nomeEndereco = $item['nomeEndereco'];
            $numeroEndereco = $item['numeroEndereco'];
            $nomeBairro = $item['nomeBairro'];
            $nomeComplemento = $item['nomeComplemento'];
            $numeroCep = $item['numeroCep'];
            $nomeCidade = $item['nomeCidade'];
            $nomeUF = $item['nomeUF'];
            $codigoIbge = $item['codigoIbge'];                
        }

        $textoEnderecoEntrega =  $nomeEndereco.', '.$numeroEndereco.', '.$nomeBairro.',  '.$numeroCep.', '.$nomeCidade.', '.$nomeUF;
    } else {
        $textoEnderecoEntrega = 'CLIQUE AQUI e informe seu endereço.';
    }
    

    // CALCULO DOS DIAS DE ENTREGA    
    if ($codigoIbge == $codigoIbgeLocal) {
        $dataPrevisaoEntrega    =   date('d/m/Y', strtotime('+'.$diasEntregaLocal.' days'));
        $dataPrevisao    =   date('Y-m-d', strtotime('+'.$diasEntregaLocal.' days'));
    } else {
        $dataPrevisaoEntrega    =   date('d/m/Y', strtotime('+'.$diasEntregaOutros.' days'));
        $dataPrevisao    =   date('Y-m-d', strtotime('+'.$diasEntregaOutros.' days'));
    }
?>

<input type="hidden" form="frmCompraProduto" name="nomeEnderecoEntrega" id="nomeEnderecoEntrega" value="<?=$nomeEndereco?>" />
<input type="hidden" form="frmCompraProduto" name="numeroEnderecoEntrega" id="numeroEnderecoEntrega"  value="<?=$numeroEndereco?>" />
<input type="hidden" form="frmCompraProduto" name="nomeBairroEntrega" id="nomeBairroEntrega"  value="<?=$nomeBairro?>" />
<input type="hidden" form="frmCompraProduto" name="nomeComplementoEntrega" id="nomeComplementoEntrega"  value="<?=$nomeComplemento?>" />
<input type="hidden" form="frmCompraProduto" name="numeroCepEntrega" id="numeroCepEntrega"  value="<?=$numeroCep?>" />
<input type="hidden" form="frmCompraProduto" name="nomeCidadeEntrega" id="nomeCidadeEntrega"  value="<?=$nomeCidade?>" />
<input type="hidden" form="frmCompraProduto" name="nomeUFEntrega" id="nomeUFEntrega"  value="<?=$nomeUF?>" />
<input type="hidden" form="frmCompraProduto" name="codigoIbgeEntrega" id="codigoIbgeEntrega"  value="<?=$codigoIbge?>" />
<input type="hidden" form="frmCompraProduto" name="dataEntrega" id="dataEntrega"  value="<?=$dataPrevisao?>" />

