<?php
    $idCarrinho = (isset($_REQUEST['idCarrinho'])) ? $_REQUEST['idCarrinho'] : null; 
    $idCompra = (isset($_REQUEST['idCompra']) && ($_REQUEST['idCompra'] <> '')) ? $_REQUEST['idCompra'] : null; 
    $idParceiro = (isset($_REQUEST['idParceiro'])) ? $_REQUEST['idParceiro'] : null; 
    $codigoGiftCard = (isset($_REQUEST['codigoGiftCard'])) ? $_REQUEST['codigoGiftCard'] : null; 

    // RETORNO DO FINAL DA COMPRA
    $textoRetorno = (isset($_REQUEST['textoRetorno'])) ? $_REQUEST['textoRetorno'] : null; 
    $terxtoErroInterno = (isset($_REQUEST['terxtoErroInterno'])) ? $_REQUEST['terxtoErroInterno'] : null; 
    $ativoCompraRealizada = (isset($_REQUEST['ativoCompraRealizada'])) ? $_REQUEST['ativoCompraRealizada'] : null;   
    $codigoVoucher = (isset($_REQUEST['codigoVoucher'])) ? $_REQUEST['codigoVoucher'] : null;   
    $codigoValeCompras = (isset($_REQUEST['codigoValeCompras'])) ? $_REQUEST['codigoValeCompras'] : null;   
    $tipoTransacao = (isset($_REQUEST['tipoTransacao'])) ? $_REQUEST['tipoTransacao'] : null;   
     

    $dadosCompraProduto = compraProdutoConsultar('s', null, null, $idCompra);

    $valorLiquido       =   formatar_moeda($dadosCompraProduto[0]['valorLiquido'],2);
    $valorFrete         =   formatar_moeda($dadosCompraProduto[0]['valorFrete'],2);
    $valorTotalCompra   =   formatar_moeda($dadosCompraProduto[0]['valorTotalCompra'],2);

    $dadosParceiro = parceiros($dadosCompraProduto[0]['idParceiro']);

    $ativoRetirada = 0;
    $textoEnderecoEntrega = 'Não informado';

    if ($dadosParceiro) {
        $ativoRetirada  =   $dadosParceiro[0]['ativoRetirada'];
        $ativoEntrega  =   $dadosParceiro[0]['ativoEntrega'];
        $diasEntregaLocal  = $dadosParceiro[0]['diasEntregaLocal'];    
        $diasEntregaOutros  = $dadosParceiro[0]['diasEntregaOutros'];    
        $codigoIbgeLocal  = $dadosParceiro[0]['codigoIbgeLocal'];    
        $numeroCelular  = $dadosParceiro[0]['numeroCelular'];    

        if ($ativoEntrega == 1){
            // ENTREGA     
            $dadosEndereco = usuarioEndereco($token, 'S', null, null, null, null, null, null, null, null, null);    

            foreach ($dadosEndereco as $item) {
                $idEndereco = $item['idEndereco'];
                $nomeEndereco = $item['nomeEndereco'];
                $numeroEndereco = $item['numeroEndereco'];
                $nomeBairro = $item['nomeBairro'];
                $nomeComplemento = $item['nomeComplemento'];
                $numeroCep = $item['numeroCep'];
                $nomeCidade = $item['nomeCidade'];
                $nomeUF = $item['nomeUF'];
                $codigoIbge = $item['codigoIbge'];
            }
            
            $textoEnderecoEntrega =  $nomeEndereco.', '.$numeroEndereco.', '.$nomeBairro.',  '.$numeroCep.', '.$nomeCidade.', '.$nomeUF;
        }
    }

    if ($ativoEntrega){
        // CALCULO DOS DIAS DE ENTREGA    
        if ($codigoIbge == $codigoIbgeLocal) {
            $dataPrevisaoEntrega    =   date('d/m/Y', strtotime('+'.$diasEntregaLocal.' days'));
        } else {
            $dataPrevisaoEntrega    =   date('d/m/Y', strtotime('+'.$diasEntregaOutros.' days'));
        }
    }   
    

    $separador      =  '----------------------------------------------------------------';
    $textoWhatsapp  =  $separador.'\n';
    $textoWhatsapp  =  $textoWhatsapp.'Olá, Realizei uma compra em sua *Loja Trocados!*\n';
    $textoWhatsapp  =  $textoWhatsapp.$separador.'\n';
    $textoWhatsapp  =  $textoWhatsapp.'*Código da Compra:* '.$idCompra.'\n';
    $textoWhatsapp  =  $textoWhatsapp.$separador.'\n';

    if ($dadosCompraProduto){
        $textoWhatsapp =  $textoWhatsapp.'*Produtos:* \n\n';
        $textoProdutos = '';
        foreach ($dadosCompraProduto as $item){
            $textoProdutos = $textoProdutos.'*Item:* '.$item['nomeProduto'].'\n';
            $textoProdutos = $textoProdutos.'*Qtd:* '.$item['numeroQuantidade'].'\n';
            $textoProdutos = $textoProdutos.'*Valor:*  R$'.$item['valorProduto'].'\n';
            $textoProdutos = $textoProdutos.'*Total:*  R$'.($item['numeroQuantidade'] * $item['valorProduto']).'\n\n';            
        }
        $textoWhatsapp =  $textoWhatsapp.$textoProdutos.'\n';
    }
    $textoWhatsapp  =  $textoWhatsapp.$separador.'\n';
    $textoWhatsapp  =  $textoWhatsapp.'*Valor Produtos:* R$ '.$dadosCompraProduto[0]['valorLiquido'].'\n';
    if($ativoEntrega){
        $textoWhatsapp  =  $textoWhatsapp.'*Valor Taxa:* R$ '.$dadosCompraProduto[0]['valorFrete'].'\n';
    }
    $textoWhatsapp  =  $textoWhatsapp.'*Valor Total:* R$ '.$dadosCompraProduto[0]['valorTotalCompra'].'\n';
    $textoWhatsapp  =  $textoWhatsapp.$separador.'\n';

    if($ativoEntrega){
        $textoWhatsapp =  $textoWhatsapp.'*Local para entrega:* \n';
        $textoWhatsapp =  $textoWhatsapp.'Endereço: '.$nomeEndereco.'\n';
        $textoWhatsapp =  $textoWhatsapp.'Número: '.$numeroEndereco.'\n';
        $textoWhatsapp =  $textoWhatsapp.'Bairro: '.$nomeBairro.'\n';
        $textoWhatsapp =  $textoWhatsapp.'CEP: '.$numeroCep.'\n';
        $textoWhatsapp =  $textoWhatsapp.'Cidade: '.$nomeCidade.'\n';
        $textoWhatsapp =  $textoWhatsapp.'UF: '.$nomeUF.'\n';        
        $textoWhatsapp  =  $textoWhatsapp.$separador;
    }
    
?>