
<label class="labelRetirada">Local para Entrega</label>
<div class="p-2">   
    <div class="card_desc_principal white shadow  bg-white rounded">
        <div class="card-produto">                  
            <label class="label-resumo">            
                <div class="row ">
                    <div class="col-4 center">                                        
                        <img style="width:40%; min-width: 50px" src="img/icons-maps.svg" />
                    </div>                
                    <div class="col-8 "> 
                    <span id="textoEndereco"><?=$textoEnderecoEntrega?></span>   
                    <br>
                    <span id="textoEndereco"><strong>Previsão de Entrega: </strong><?=$dataPrevisaoEntrega?></span>                                                                         
                    </div>                
                </div>                         
        </div>
    </div>
</div>      
