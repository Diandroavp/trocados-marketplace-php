<?php foreach($dadosCompraProduto as $item) {?>
<div class="p-2" >        
    <div class="card card_desc_principal white shadow">            
        <div class="card-produto">
            
            <div class="">
                <label class="label-resumo">
                    <div class="row ">
                        <div class="col-5 center">                                        
                            <img style="width:100%; min-width: 100px" src="<?=$item['nomeImagem']?>"  />
                        </div>                
                        <div class="col-7 ">                                        
                            <?=$item['nomeProduto']?>
                        </div>                
                    </div> 
                </label>
            </div>                                            

            <div class="row ">
                <div class="col-7"> 
                Quantidade:                                       
                    <span >
                         <?=$item["numeroQuantidade"]?>
                    </span>
                </div>                
                <div class="col-5 ">                                        
                    <div  class=" center" >
                        <strong>
                            <?=formatar_moeda($item["numeroQuantidade"] * $item["valorProduto"],2)?>
                        </strong>
                    </div>
                </div>                
            </div> 
        </div>
    </div>    
</div>
<?php }?>  