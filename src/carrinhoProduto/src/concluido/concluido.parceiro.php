<?php foreach ($dadosParceiro as $item) {?>
    <div class="p-2">                        
        <div class="card_desc_principal white shadow  bg-white rounded">
            <div class="card-produto">                 
                <label class="label-resumo">
                <div class="row ">
                    <div class="col-4 center">                                        
                        <img style="width:40%; min-width: 50px" src="<?=$item['nomeImagem']?>" />
                    </div>                
                    <div class="col-8 "> 
                        <div><?=$item['nomeParceiro']?></div>
                        <?php if ($item['ativoRetirada']) {?>                            
                            <div>Endereço: <?=$item['textoEndereco']?></div>                            
                        <?php }?>
                    </div>                
                </div>                      
            </div>
            <?php if ($item['ativoRetirada']) {?>
                <div class="card-produto">                                          
                        <span><b>Importante!</b></span>
                        <span>Fique atento ao horário de funcionamento do estabelecimento do parceiro!</span>                            
                </div>
            <?php }?>
             
        </div>
    </div>

<?php }?>