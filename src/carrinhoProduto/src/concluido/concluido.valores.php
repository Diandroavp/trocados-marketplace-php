
    <div class=" p-2">
        <div class="card card_desc_principal white">            
            <div class="card-produto">      
                <div class="row ">
                    <div class=" col-6">                        
                            <label class="label-resumo">Valor Produtos: </label>                        
                    </div>
                    <div class=" col-6">
                        <strong>
                                <div class="valorProdutoDesconto" id="valorProdutos" >
                                    <?=$valorLiquido?>
                                </div> 
                                
                        </strong>       
                    </div>
                </div> 
                <?php if ($ativoEntrega) {?>
                    <div class="row " id="divFrete">
                        <div class=" col-6">                        
                                <label class="label-resumo">Valor Frete: </label>                        
                        </div>
                        <div class=" col-6">
                            <strong>
                                    <div class="valorProdutoDesconto" id="valorFrete" >
                                        <?=$valorFrete?>
                                    </div> 
                                    
                            </strong>       
                        </div>
                    </div> 
                <?php }?>
                <div class="row ">
                    <div class=" col-6">                        
                            <label class="label-resumo">Valor Total: </label>                        
                    </div>
                    <div class=" col-6">
                        <strong>
                                <div class="valorProdutoDesconto" id="valorTotal" >
                                    <?=$valorTotalCompra?>
                                </div>                                 
                        </strong>       
                    </div>
                </div> 
            </div>
        </div>    
    </div>   