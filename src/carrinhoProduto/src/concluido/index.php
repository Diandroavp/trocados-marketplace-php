<?php include "concluido.controller.php"?>
    <!--CARD PRINCIPAL-->
    <div class="col-12">        
        <label class="labelCategoria">Minha Compra</label>
        
        <!-- TIPO DESCRICAO -->
        <?php      
            if ($ativoCompraRealizada) {
                include "concluido.texto.retorno.php" ; 
            } else {
                include "concluido.texto.retorno.falha.php" ; 
            }
            
        ?>

        <label class="labelCategoria">Produtos</label>
        <?php include "concluido.produtos.php";?>

        <label class="labelRetirada">Parceiro</label>
        <?php include "concluido.parceiro.php";?>


        
        <?php 
            if ($ativoEntrega == 1) {
                include "concluido.entrega.php";
            }            
        ?>

        <label class="labelCategoria">Valores da Compra</label>
        <?php include "concluido.valores.php";?>


        <form role="form" id="frmCompraProduto" name="frmCompraProduto" action="?" method="post" >            
            <input type="hidden" name="_route" id="_route" value="" />                               
        </form>

</div>

<!-- CONFIRMAR COMPRA-->
<div class="blocoConfirmar fixarRodape p-3">
<div class="row">
        <?php if (!$ativo_uso_app) { ?>
            <div class="col-8">                    
                <button id="btnConfirmar" type="button" class="btn btn-block btn-lg btnConfirmacao center" >
                    Comprar Mais
                </button>
            </div>        
            <div class="col-4">                    
                <button type="button"  onclick="return enviaFormularioSimples('frmSair', 0)"  class="btn btn-block btn-lg btnSair center" >
                    Sair
                </button>
            </div>
        <?php } else { ?>
            <div class="col-12">                    
                <button id="btnConfirmar" type="button" class="btn btn-block btn-lg btnConfirmacao center" >
                    Comprar Mais
                </button>
            </div>        
        <?php } ?>            
    </div>        
</div> 

<form class="pesquisar" id="frmSair">                       
</form> 

<?php  if ($ativoCompraRealizada) { ?>
    <audio id="notificacao" preload="auto">
        <source src="audio/trocados.mp3" type="audio/mpeg">
    </audio>
    <audio id="notificacao_irra" preload="auto">
        <source src="audio/trocados_irra.mp3" type="audio/mpeg">
    </audio>
    <audio id="notificacao_arrasou" preload="auto">
        <source src="audio/trocados_arrasou.mp3" type="audio/mpeg">
    </audio>
    <audio id="obrigado-amigo-voce-e-um-amigo" preload="auto">
        <source src="audio/obrigado-amigo-voce-e-um-amigo.mp3" type="audio/mpeg">
    </audio>
    <audio id="ratinhooo_" preload="auto">
        <source src="audio/ratinhooo_.mp3" type="audio/mpeg">
    </audio>
<?php } ?>

<script>
    var botao = document.querySelector('#btnConfirmar');

    botao.addEventListener('click', function (event) {
        loaderTrocadosJs();
        enviaFormularioSimples('frmCompraProduto');       
        return true;               
    });


    function enviarWhatsapp(numero, texto) {
        texto = window.encodeURIComponent(texto);
        window.open('https://api.whatsapp.com/send/?phone=' + numero + '&text=' + texto + '&app_absent=0');
    }

    var ativo_uso_app = '<?=$ativo_uso_app?>';
    
    if (ativo_uso_app != 1) {
        if (Math.floor(Math.random() * 20) == 13) {
            $('#obrigado-amigo-voce-e-um-amigo').trigger('play');
        } else if (Math.floor(Math.random() * 20) == 13) {
            $('#ratinhooo_').trigger('play');
        } else if (Math.floor(Math.random() * 20) == 13) {
            $('#notificacao_irra').trigger('play');
        } else if (Math.floor(Math.random() * 20) == 19) {
            $('#notificacao_arrasou').trigger('play');
        } else {
            $('#notificacao').trigger('play');
        }   
    }
    
    setTimeout(redirecionarFinal, 60000);    

    function redirecionarFinal() {
        let ativo_uso_app = '<?=$ativo_uso_app?>';

        if (ativo_uso_app) {
            loaderTrocadosJs();
            enviaFormularioSimples('frmCompraProduto');    
        } else {
            loaderTrocadosJs();
            enviaFormularioSimples('frmSair', 0);
        }
        
    }
     
</script>
