<div class="p-3">    
    <!-- TEXTO DESCRITIVO-->   
    <div class="card">
        <div class="card-body text-center">                          
            <h5><b><?=$textoRetorno?></b></5>
            <div class="row p-3">
                <?php if($tipoTransacao == 'cd') {?>
                    <div class="col-12 ">                    
                        <img style="width:100%; max-width: 100px " src="img/sucesso.png" />
                    </div>  
                <?php } else if($tipoTransacao == 'vc') {?>
                    <div class="col-12 ">                    
                        <h2><center><?=$codigoValeCompras?></center></h2>
                    </div>
                    <h5>Vá até o endereço do parceiro, informe o código acima para confirmar a aquisição!</h5>                         

                <?php }?>
            </div>   
            <!--
            <p>                
                <h4><b>Dúvidas?</b></h4>
                <span>Em caso de dúvida, entre em contato pelo nosso canal de Atendimento ( Fale Conosco )!!</span>
            </p>
                -->     

                
            
            <?php if($numeroCelular) {?>
                <p>                
                    <h4><b>Fale com a loja parceira</b></h4>
                    <span>Envie uma mensagem, via whatsapp, para a loja saber mais rapidamente sobre seu pedido.</span>
                </p>

                <div class="row">        
                    <div class="col-12">                    
                        <button id="btnWhatsapp" type="button" class="btn btn-block btn-lg btnWhatsapp center" onclick="return enviarWhatsapp('55<?=$numeroCelular?>', '<?=$textoWhatsapp?>')" >
                            Enviar por Whatsapp
                        </button>
                    </div>                
                </div>    
            <?php }?>
        </div>
    </div>
</div>

