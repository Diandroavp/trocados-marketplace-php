function montarRetirada(token) {

  var dadosParametro = {
      nomeFuncao : 'retirada',
      token: token
    };
    
    $.ajax({
    url: './services/carrinhoProduto/carrinhoProduto.service.php',
      dataType: "json",
      data: dadosParametro,
      success: function (response) { 
        if (response[0].ativoEntrega == 1) { 
          $("#divRetirada").css('display', 'block');            
          
          $("#textoEndereco").text(response[0].textoEndereco);
          $("#nomeParceiro").val(response[0].nomeParceiro);
          $("#dataEntrega").text(response[0].dataEntrega);
          $("#horaEntregaInicial").text(response[0].horaEntregaInicial);
          
        } else {
          $("#divRetirada").css('display', 'none');            
        }        
          
      }

  })

}
  
  function listarProdutos(token) {

    var dadosParametro = {
        nomeFuncao : 'listar',
        token: token
      };
      
      $.ajax({
      url: './services/carrinhoProduto/carrinhoProduto.service.php',
        dataType: "json",
        data: dadosParametro,
        success: function (response) { 
          $("#overlay_produto").css('display', 'none');     

          if (response[0].ativoRetorno == 1) {            
            $("#btnConfirmar").prop('disabled', false);
            
            $("#carrinhoProduto").html(response[0].divProduto);
            $("#divParceiro").html(response[0].divParceiro);       

            
            $("#valorTotalBotao").html(response[0].valorTotal);          
            $("#valorTotal").html(response[0].valorTotal);
            $("#valorTotalCarrinho").val(response[0].valorTotalCarrinho);

            $("#valorFrete").html(response[0].valorFrete);
            $("#valorFreteCarrinho").val(response[0].valorFreteCarrinho);

            $("#valorProdutos").html(response[0].valorProdutos);
            $("#valorTotalProdutos").val(response[0].valorTotalProdutos);

            
            $("#overlay_produto").css('display', 'none');  

            $("#idCarrinho_carrinho").val(response[0].idCarrinho);
            $("#idParceiro_carrinho").val(response[0].idParceiro);
            $("#token_carrinho").val(token);


            $("#_route_carrinho").val('comprar');
            $("#_subroute_carrinho").val('concluido');

            if (response[0].ativoEntrega == 1){              
              //$("#_route_origem_carrinho").val(response[0].nomeTipoProduto);              
              //$("#divFrete").css('display', 'block');                
            } else {              
              $("#divFrete").css('display', 'none'); 
            }

            $("#carrinhoProduto").css('padding-bottom', '5px');
          } else {
            $("#btnConfirmar").prop('disabled', true);
            $("#semProduto").css('display', 'block');          
          }

          
            
        }
  
    })
  
}

function removerProdutos(token, idCarrinho, idProduto) {
    var dadosParametro = {
        nomeFuncao : 'remover',
        idCarrinho: idCarrinho,
        idProduto: idProduto,
        token: token
      };
      
      $.ajax({
      url: './services/carrinhoProduto/carrinhoProduto.service.php',
        dataType: "json",
        data: dadosParametro,
        success: function (response) {  
            if(response[0].ativoRetorno){
                $("#valorTotal").html(response[0].valorTotal);
                $("#valorTotalBotao").html(response[0].valorTotal);
                $("#valorTotalCarrinho").val(response[0].valorTotalCarrinho);
                $( "#produto"+idCarrinho+idProduto ).remove();
            }
        }
  
    })
  
  }

  function adicionarQuantidade(token, idCarrinho, idProduto) {
    var dadosParametro = {
        nomeFuncao : 'addQuantidade',
        idCarrinho: idCarrinho,
        idProduto: idProduto,
        token: token
      };
      
      $.ajax({
      url: './services/carrinhoProduto/carrinhoProduto.service.php',
        dataType: "json",
        data: dadosParametro,
        success: function (response) {  
            if(response[0].ativoRetorno == 1){
                $("#valorTotal").html(response[0].valorTotal);
                $("#valorTotalBotao").html(response[0].valorTotal);
                $("#valorTotalCarrinho").val(response[0].valorTotalCarrinho);
                $("#numeroQuantidade" + idProduto).html(response[0].numeroQuantidade);
                $("#valorProduto" + idProduto).html(response[0].valorProduto);
            } else {
              Swal.fire({
                icon: 'error',
                title: 'Oops...',
                confirmButtonColor: '#33CB00',
                text: response[0].textoRetorno                
            })   
            }
        }
  
    })
  
  }

  function removerQuantidade(token, idCarrinho, idProduto) {
    
    let numeroQuantidade = parseInt($("#numeroQuantidade" + idProduto).text());
        
    if (numeroQuantidade > 1) {
        var dadosParametro = {
            nomeFuncao : 'remQuantidade',
            idCarrinho: idCarrinho,
            idProduto: idProduto,
            token: token
          };
          
          $.ajax({
          url: './services/carrinhoProduto/carrinhoProduto.service.php',
            dataType: "json",
            data: dadosParametro,
            success: function (response) {  
                if(response[0].ativoRetorno){
                    $("#valorTotal").html(response[0].valorTotal);
                    $("#valorTotalBotao").html(response[0].valorTotal);
                    $("#valorTotalCarrinho").val(response[0].valorTotalCarrinho);
                    $("#numeroQuantidade" + idProduto).html(response[0].numeroQuantidade);
                    $("#valorProduto" + idProduto).html(response[0].valorProduto);
                }    
            }
      
        })
    }
    
  
  }

  function esvaziarCarrinho(token) {

    var dadosParametro = {
      nomeFuncao : 'esvaziar',
      token: token      
    };

    Swal.fire({
      title: 'Esvaziar o carrinho?',
      text: "Você retornarar para a tela inicial",            
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#33CB00',
      cancelButtonColor: '#ccc',
      confirmButtonText: 'Sim, Esvaziar!',
      cancelButtonText: 'Cancelar'
      }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          url: './services/carrinho/carrinho.service.php',
            dataType: "json",
            data: dadosParametro,
            success: function (response) {  
                if(response[0].ativoRetorno){
                  loaderTrocadosJs();
                  enviaFormularioSimples('frmVoltar');                  
                  return true;    
                } else {
                  Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    confirmButtonColor: '#33CB00',
                    text: response[0].textoRetorno                
                })   
                }    
            }
      
        })            
      }
  })       


    
        
          
         
    
    
  
  }

  
  
  