<!-- Pills navs -->
<ul class="nav nav-pills nav-fill mb-4 fixarTab" id="ex1" role="tablist">
  <li class="nav-item" role="presentation">
    <a
      onclick="enviaFormularioSimples('frmHome')"
      class="nav-link nav-link-rodape"
      id="ex2-tab-1"
      data-mdb-toggle="pill"
      href="#ex2-pills-1"
      role="tab"
      aria-controls="ex2-pills-1"
      aria-selected="true"
      ><i class="fas fa-store fa-lg"></i>
      <br><span class="label-tab">Mercado</span></a
    >
  </li>
  <li class="nav-item" role="presentation">
    <a
      onclick="enviaFormularioSimples('frmBuscar')"
      class="nav-link nav-link-rodape"
      id="ex2-tab-2"
      data-mdb-toggle="pill"
      href="#ex2-pills-2"
      role="tab"
      aria-controls="ex2-pills-2"
      aria-selected="false"
      ><i class="fa fa-search fa-lg"></i>
      <br><span class="label-tab">Buscar</span></a
    >
  </li>
  <li class="nav-item" role="presentation">
    <a
      onclick="enviaFormularioSimples('frmPedidos')"
      class="nav-link nav-link-rodape"
      id="ex2-tab-2"
      data-mdb-toggle="pill"
      href="#ex2-pills-2"
      role="tab"
      aria-controls="ex2-pills-2"
      aria-selected="false"
      ><i class="fas fa-shopping-basket fa-lg"></i>
      <br><span class="label-tab">Pedidos</span></a
    >
  </li>
  <li class="nav-item" role="presentation">
    <a
    onclick="enviaFormularioSimples('frmPerfil')"
      class="nav-link nav-link-rodape"
      id="ex2-tab-3"
      data-mdb-toggle="pill"
      href="#ex2-pills-3"
      role="tab"
      aria-controls="ex2-pills-3"
      aria-selected="false"
      >
      <i class="fas fa-user-alt fa-lg"></i>      
      <br><span class="label-tab"><?=$nomeUsuarioCurto?></span>
      </a
    >
  </li>
</ul>


<!-- Pills content -->
<div class="tab-content" id="ex2-content">
  <div
    class="tab-pane fade show active"
    id="ex2-pills-1"
    role="tabpanel"
    aria-labelledby="ex2-tab-1"
  >
    Tab 1 content
  </div>
  <div
    class="tab-pane fade"
    id="ex2-pills-2"
    role="tabpanel"
    aria-labelledby="ex2-tab-2"
  >
    Tab 2 content
  </div>
  <div
    class="tab-pane fade"
    id="ex2-pills-3"
    role="tabpanel"
    aria-labelledby="ex2-tab-3"
  >
    Tab 3 content
  </div>
</div>
<!-- Pills content -->

<!--
<form  role="form" id="frmBuscar" action="" method="post" >		   
  <input type="hidden" name="_route" " value="homePedidos" />                          
</form>


<form role="form" id="frmPedidos" action="" method="post" >		   
  <input type="hidden" name="_route" " value="homePedidos" />                          
</form>

<form role="form" id="frmPerfil" action="" method="post" >		   
  <input type="hidden" name="_route" " value="homePedidos" />                          
</form>
 -->