<footer class="rodape pb-5">
    <div class="container pb-5">
        <div class="col-12" style="margin-top: 30px;">
            <div class="row">
                <!-- <div class="col-2" id="logo-footer">
                    <img src="img/logo-trocados-green.svg" alt="" style="max-width: 60px"
                        onclick="enviaFormularioSimples('frmHome')">

                </div> -->

                <div class="col-12 text-center">
                    <div class="label-footer">Redes <b>Sociais</b></div>
                    <a hre="https://www.facebook.com/apptrocados">
                        <img class="redes-sociais" src="img/facebook.svg">
                    </a>
                    <a href="https://www.instagram.com/trocados/">
                        <img class="redes-sociais" src="img/instagram.svg">
                    </a>
                    <a href="https://www.youtube.com/channel/UCeHou07YtxIOtx0tVJdjv-Q">
                        <img class="redes-sociais" style="width: 34px;" src="img/youtube.svg">
                    </a>

                </div>
                <div class="col-12 copyright">
                    Copyright© 2019 Trocados. Todos os direitos reservados.
                </div>
            </div>
        </div>


</footer>