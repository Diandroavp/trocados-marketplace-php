
<!--CARD PRINCIPAL-->
<div class="altura-min" > 
    <!-- CATEGORIAS -->
    <?php 
        

        if ($ativoCategorias){
            include "principal.categoria.php"; 
        }
    ?>   
    <!-- CONTAGEM REGRESSIVA -->
    <?php 
        if ($ativoRegressiva && $ativoContagemRegressiva){
          include "principal.contagem.regressiva.php";
        }            
    ?> 

    <!-- SLIDE PRINCIPAL -->
    <?php 
        if ($ativoSlidePrincipal) {
            include "principal.banner.principal.slide.php";
        }        
    ?> 

    <!-- BANNER PRINCIPAL -->
    <?php 
        if ($ativoBannerPrincipal) {
            include "principal.banner.principal.php";
        }        
    ?>   

     <!-- CATEGORIA POR VALOR -->
    <?php 
        if ($ativoCategoriaPorValor) {?>       
        <div class="p-2">    
            <label class="labelCategoria">Categoria por <b>Valores</b></label>    
        </div>
        
    <?php   
        include "principal.categoria.valor.php"; 
        }
    ?>
                                   
    
    <!-- HOME PARCEIROS -->
    <?php         
        if ($ativoParceirosHome){                  
            // ABRINDO OS GRUPOS DE PARCEIROS
            $dadosParceirosAtivos = parceirosAtivos($token, null, null, 1, $ativo_trocados);

            
            if ($dadosParceirosAtivos) {
                include "principal.parceiros.ativos.php";
            }
        }        
    ?>

    <!-- PARCEIROS -->
    <?php         
        if ($ativoParceirosGrupo){                  
            // ABRINDO OS GRUPOS DE PARCEIROS
            $dadosParceirosGrupos = parceirosGrupo(null);
            if ($dadosParceirosGrupos) {
                include "principal.parceiros.grupo.php";
            }
        }        
    ?>

    
</div>

<script src="src/principal/js/principal.js"></script>

