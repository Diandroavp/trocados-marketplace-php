<div class="container ">
	<div class="">
        <div class="row">
            <div class="col-lg-8 mx-auto">		   		
			<div id="clock" class="countdown"></div>			
            </div>
        </div>
    </div>
</div>
<script>
	
	$(function () {
	$('#clock').countdown('2020/12/21 10:00').on('update.countdown', function(event) {		
	var $this = $(this).html(event.strftime(''
		+ '<div class="rounded bg-gradient-3  text-center">'
		+ '<p class="mb-0 text-uppercase">Loja Trocados no ar em...</p>'
		+ '<span class="h1 font-weight-bold">%D</span> Dia%!d'
		+ '<span class="h1 font-weight-bold">%H</span> Hr'
		+ '<span class="h1 font-weight-bold">%M</span> Min'
		+ '<span class="h1 font-weight-bold">%S</span> S'
		+ '</div>'));
	});	
	
});


</script>