<div class="">    
    <div class="slick-container ">
      <div class="slick-2 ">
	        <?php foreach($dadosParceiroProduto as $item) {?>            
            <a href="#" class="slide " onclick="enviaFormularioSimples('frmParceiroProduto<?=$item['idProduto']?>')" >
              <div class="slide-content shadow  bg-white rounded" >
                <div class="product-grid8 centralizarImagemDiv1">
                    <div class="product-image8 centralizarImagemDiv2">                        
                            <img class="pic-1 centralizarImagem" src="<?=$item['nomeImagem']?>">
                            <img class="pic-2 centralizarImagem" src="<?=$item['nomeImagemSecundaria']?>">
                        <?php if ($item['percentualDesconto'] && ($item['percentualDesconto'] > 0)) { ?>                    
                          <span class="product-discount-label">-<?=$item['percentualDesconto']?>%</span>
                        <?php }?>
                    </div>
                    <div class="product-content">
                        <span class="product-shipping" title="<?=$item['nomeProduto']?>" ><?=$item['nomeProduto']?></span>                          
                        <div class="price">R$ <?=$item['valorProduto']?>
                            <?php if ($item['percentualDesconto'] && ($item['percentualDesconto'] > 0)) { ?>
                              <span>R$ <?=$item['valorOriginal']?></span>
                            <?php }?>
                        </div>                        
                    </div>
                    <span class="titulo-parceiro" >Oferecido por: </span>    
                    <span class="nome-parceiro" ><?=$item['nomeParceiro']?></span>                          
                </div>
              </div>
            </a>                        
		      <?}?>   		  
      </div>
    </div>  
</div>

<?php foreach($dadosParceiroProduto as $item) {?>
	<form role="form" id="frmParceiroProduto<?=$item['idProduto']?>" name="frmParceiroProduto<?=$item['idProduto']?>" action="" method="post" >
		   
		<input type="hidden" name="idProduto"  value="<?=$item['idProduto']?>" />    
		<input type="hidden" name="_route"  value="<?=tipoProduto($item['tipoProduto'])?>" />                            
	</form>
<?}?>