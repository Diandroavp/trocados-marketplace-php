<?php if ($tipoApresentacaoGrid == 'rolagem') {?>

<div class="">    
    <div class="slick-container">
      <div class="slick-2">
          <?php foreach($dadosprodutosAtivosHome as $itemProduto) {?>            
            <div class="slide-content shadow  bg-white rounded" >
              <a href="#" class="slide " onclick="enviaFormularioSimples('frmProduto<?=$itemProduto['idProduto']?>')" >                
                <div class="product-grid8 ">
                    <div class="product-image8 centralizarImagemDiv1 center">                        
                            <img class="pic-1 centralizarImagem center" src="<?=$itemProduto['nomeImagem']?>" >
                            <img class="pic-2 centralizarImagem center" src="<?=$itemProduto['nomeImagemSecundaria']?>" >
                        <?php if ($itemProduto['percentualDesconto'] && ($itemProduto['percentualDesconto'] > 0)) { ?>                    
                          <span class="product-discount-label">-<?=$itemProduto['percentualDesconto']?>%</span>
                        <?php }?>
                    </div>
                    <div class="product-content">
                        <span class="product-shipping" title="<?=$itemProduto['nomeProduto']?>" ><?=$itemProduto['nomeProduto']?></span>                          
                        <div class="price"> <?=formatar_moeda($itemProduto['valorProduto'],2)?>                            
                            <?php if ($itemProduto['percentualDesconto'] && ($itemProduto['percentualDesconto'] > 0)) { ?>
                              <span><?=formatar_moeda($itemProduto['valorOriginal'],2)?></span>                              
                            <?php }?>
                            <br>
                            <?php if ($itemProduto['ativoUsarCoin']) { ?>
                              <?=formatar_tcoin($itemProduto['valorProdutoCoin'],2)?>                            
                              <?php if ($itemProduto['percentualCoinDesconto'] && ($itemProduto['percentualCoinDesconto'] > 0)) { ?>
                                    <span>(<?=formatar_tcoin($itemProduto['valorCoinOriginal'],2)?>)</span>
                              <?php }?>                          
                            <?php } ?> 
                        </div>                                                
                    </div>                                                                                  
                </div>                
              </a>
              <!--<button type="button" class="close verde text-right" onclick="return addProduto('<?=$itemProduto['idProduto']?>', '<?=$itemProduto['idParceiro']?>', '<?=$token?>', '<?=$itemProduto['nomeImagem']?>', '<?=$itemProduto['nomeProduto']?>')" > 
                <span aria-hidden="true" class="" ><i class="fa fa-cart-plus" > </i></span><span class="sr-only" >Adicionar</span>
              </button>-->
              <!--<span class="titulo-parceiro" >Oferecido por: </span> -->
              <span class="nome-parceiro" ><?=$itemProduto['nomeParceiro']?></span>  
              <button 
                type="button" 
                class="btn btn-trocados center"                   
                style="width: 100%"
                onclick="return addProduto('<?=$itemProduto['idProduto']?>', '<?=$itemProduto['idParceiro']?>', '<?=$token?>', '<?=$itemProduto['nomeImagem']?>', '<?=$itemProduto['nomeProduto']?>')"
              > 
              <span aria-hidden="true" class="" > Adicionar <i class="fa fa-cart-plus" > </i></span>
            </button>
            </div>                          
          <?}?>   		  
      </div>
    </div>  
</div>
<?php } else if ($tipoApresentacaoGrid == 'grade') {?>
<div class="row" style="width: 100% !important; margin-left: 0px !important; margin-top 10px !important;">  
      <?php foreach($dadosprodutosAtivosHome as $itemProduto) {?>  
        <div class="col-4 col-md-3 col-sm-3 p-1 ">                  
        <div class="pad15 slide-content shadow  bg-white rounded">
            <a href="#" class=" slide" onclick="enviaFormularioSimples('frmProduto<?=$itemProduto['idProduto']?>')">              
              <div class="product-grid8 centralizarImagemDiv1">
                <img class="center" src="<?=$itemProduto['nomeImagem']?>" style="width: 100%"/>  
                <?php if ($itemProduto['percentualDesconto'] && ($itemProduto['percentualDesconto'] > 0)) { ?>                    
                          <span class="product-discount-label">-<?=$itemProduto['percentualDesconto']?>%</span>
                        <?php }?>     							                    
                <div class="product-content">
                      <span class="product-shipping" title="<?=$itemProduto['nomeProduto']?>" ><?=$itemProduto['nomeProduto']?></span>                          
                      <div class="price"> <?=formatar_moeda($itemProduto['valorProduto'],2)?>
                          <?php if ($itemProduto['percentualDesconto'] && ($itemProduto['percentualDesconto'] > 0)) { ?>
                            <span><?=formatar_moeda($itemProduto['valorOriginal'],2)?></span>                            
                          <?php }?>     
                          <br>                     
                          <?php if ($itemProduto['ativoUsarCoin']) { ?>
                           <?=formatar_tcoin($itemProduto['valorProdutoCoin'],2)?>                            
                            <?php if ($itemProduto['percentualCoinDesconto'] && ($itemProduto['percentualCoinDesconto'] > 0)) { ?>
                                  <span>(<?=formatar_tcoin($itemProduto['valorCoinOriginal'],2)?>)</span>
                            <?php }?>                          
                          <?php } ?>                   
                      </div>       
                      
                  </div>     
                </div>              					                  
            </a>
            <!--<button type="button" class="close verde text-right" onclick="return addProduto('<?=$itemProduto['idProduto']?>', '<?=$itemProduto['idParceiro']?>', '<?=$token?>', '<?=$itemProduto['nomeImagem']?>', '<?=$itemProduto['nomeProduto']?>')"> 
              <span aria-hidden="true" class="" ><i class="fa fa-cart-plus" > </i></span><span class="sr-only" >Adicionar</span>
            </button>-->
            <!--<span class="titulo-parceiro" >Oferecido por: </span>  -->
            <span class="nome-parceiro" ><!-- <?=$itemProduto['nomeParceiro']?> --></span>  
            <button 
              type="button" 
              class="btn btn-trocados center"                   
              style="width: 100%"
              onclick="return addProduto('<?=$itemProduto['idProduto']?>', '<?=$itemProduto['idParceiro']?>', '<?=$token?>', '<?=$itemProduto['nomeImagem']?>', '<?=$itemProduto['nomeProduto']?>')"
            > 
              <span aria-hidden="true" class="" > Adicionar <i class="fa fa-cart-plus" > </i></span>
            </button>
          </div>
        </div>					                  
      <?}?>   		  
  
  </div>

<?php } ?>


<?php foreach($dadosprodutosAtivosHome as $itemProduto) {?>
<form role="form" id="frmProduto<?=$itemProduto['idProduto']?>" name="frmProduto<?=$itemProduto['idProduto']?>" action="" method="post" >
     
  <input type="hidden" name="idProduto"  value="<?=$itemProduto['idProduto']?>" />    
  <input type="hidden" name="_route"  value="<?=tipoProduto($itemProduto['tipoProduto'])?>" />                          
</form>
<?}?>
