<?php
    
    global $id_usuario_pf, $valorSaldo, $nomeUsuario,  $numeroCelularUsuario, $emailUsuario, $ativoProducao;

    $idCarrinho = (isset($_REQUEST['idCarrinho'])) ? $_REQUEST['idCarrinho'] : null; 
    $idParceiro = (isset($_REQUEST['idParceiro'])) ? $_REQUEST['idParceiro'] : null;     
    /*  RETIRADA */
    $tipoAutorRetirada = (isset($_REQUEST['tipoAutorRetirada'])) ? $_REQUEST['tipoAutorRetirada'] : null;     
    $nomePessoaRetirada = (isset($_REQUEST['nomePessoaRetirada'])) ? $_REQUEST['nomePessoaRetirada'] : null; 
    $tipoDocumentoRetirada = (isset($_REQUEST['tipoDocumentoRetirada'])) ? $_REQUEST['tipoDocumentoRetirada'] : null; 
    $numeroDocumentoRetirada = (isset($_REQUEST['numeroDocumentoRetirada'])) ? $_REQUEST['numeroDocumentoRetirada'] : null; 


    
    /*  ENTREGA */
    $dataEntrega = (isset($_REQUEST['dataEntrega'])) ? $_REQUEST['dataEntrega'] : null;  
    $dataEntrega = convertDataParaBanco($dataEntrega, 'Y-m-d');    
    $nomeBairroEntrega = (isset($_REQUEST['nomeBairroEntrega'])) ? $_REQUEST['nomeBairroEntrega'] : null;  
    $nomeComplementoEntrega = (isset($_REQUEST['nomeComplementoEntrega'])) ? $_REQUEST['nomeComplementoEntrega'] : null;  
    $numeroEnderecoEntrega = (isset($_REQUEST['numeroEnderecoEntrega'])) ? $_REQUEST['numeroEnderecoEntrega'] : null;  
    $nomeEnderecoEntrega = (isset($_REQUEST['nomeEnderecoEntrega'])) ? $_REQUEST['nomeEnderecoEntrega'] : null;  
    $numeroCepEntrega = (isset($_REQUEST['numeroCepEntrega'])) ? $_REQUEST['numeroCepEntrega'] : null;  
    $numeroCepEntrega = limpaCaracter($numeroCepEntrega, true);
    $nomeCidadeEntrega = (isset($_REQUEST['nomeCidadeEntrega'])) ? $_REQUEST['nomeCidadeEntrega'] : null;
    $nomeUFEntrega = (isset($_REQUEST['nomeUFEntrega'])) ? $_REQUEST['nomeUFEntrega'] : null;
    $codigoIbgeEntrega = (isset($_REQUEST['codigoIbgeEntrega'])) ? $_REQUEST['codigoIbgeEntrega'] : null;

    $_route_origem = (isset($_REQUEST['_route_origem'])) ? $_REQUEST['_route_origem'] : null; 
    $_subroute = (isset($_REQUEST['_subroute'])) ? $_REQUEST['_subroute'] : null; 
      
    // PARA TRANSAÇÃO DE RECARGA
    $numeroCelular = (isset($_REQUEST['numeroCelular'])) ? $_REQUEST['numeroCelular'] : null; 
    $numeroCelular = limpaCaracter($numeroCelular, true);    

    $nomeOperadoraRecarga = (isset($_REQUEST['nomeOperadoraRecarga'])) ? $_REQUEST['nomeOperadoraRecarga'] : null; 
    
    $token = (isset($_REQUEST['token'])) ? $_REQUEST['token'] : null; 
    $token_validacao = (isset($_REQUEST['token_validacao'])) ? $_REQUEST['token_validacao'] : null; 
    $token_validacao = substr($token_validacao,3, strlen($token_validacao) -3);

    $tag_usuario = (isset($_REQUEST['tag_usuario'])) ? $_REQUEST['tag_usuario'] : null; 

    $codigoVoucher = '';
    $codigoValeCompras = '';
    $codigoGiftCard = '';
    $codigoHash = null;
    $ativoRegistro = null;
    $textoRetorno = null;
    $quantidadeDisponivel = 0;
    $ativoCompraRealizada = 0;     
    $terxtoErroInterno = '';
    $valorTotal = 0;
    //$tipoProduto = 'vc';
    $tipoGiftCard = null;         
    $ativoControleCodigo = null;
    $ativoRetorno = 0;
    $idUnidade = null;
    $tipoTransacao = null;
    // FRETE
    $valorFrete = 0;

    try {
        // VERIFICANDO SE O SALDO É SUFICIENTE
        $dadosCarrinho = carrinho('s', $idCarrinho, null, null);

        $dadosParceiro = parceiros($idParceiro);

        foreach ($dadosParceiro as $item){
            if ($item['ativoEntrega']) {
                // ENTREGA LOCAL
                if ($item['codigoIbgeLocal'] == $codigoIbgeEntrega) {
                    $valorFrete = $item['valorFreteLocal'];                    
                } else {
                    $valorFrete = $item['valorFreteOutros'];                    
                }
            }
        }

        foreach ($dadosCarrinho as $item){
            $valorTotal = $item['valorTotal'];            
            $dataCarrinho = $item['dataCarrinho'];     
            $tipoTransacao = $item['tipoTransacao'];     
        }

        // SOMANDO FRETE
        $valorTotal = $valorTotal + $valorFrete;

        // CARRINHO NO ATO DA COMPRA
        $dadosCarrinhoProduto = carrinhoProduto('s', $id_usuario_pf, null, null, null, null, null, null);     
        include "auditoria/compra.auditoria.carrinho.php";   

        // CASO SEJA DO TIPO VALE COMPRA, GIFTCARD OU RECARGA, O SISTEMA NÃO EFETUARÁ O DÉBITO DO VALOR NA SP DO PROJETO
        //$ativoDebitarSaldo = (($tipoProduto == 'vc') || ($tipoProduto == 'g') || ($tipoProduto == 'r')) ? 0 : 1;
        $ativoDebitarSaldo = ($tipoTransacao == 'cd') ? 1 : 0; // cd - COMPRA DIRETA - vc - VALE COMPRAS

        
        // VALIDANDO A SENHA DO USUÁRIO OU O TOKEN
        if ($ativo_uso_app) {            
            $dadosLogin = validarSenha($token, $tag_usuario);            

            if ((array_key_exists("code", $dadosLogin)) && ($dadosLogin['code'] == 200)) {   
                $textoRetorno = null;
            } else if (array_key_exists("code", $dadosLogin)) {
                if ($dadosLogin['code'] <> 200) {                  
                    $textoRetorno = $dadosLogin['mensagem'];
                    $terxtoErroInterno = 'ERRO 00010';
                }   
            } else {
                $textoRetorno = 'Sua compra não foi concluída pois não conseguimos validar sua senha!';
                $terxtoErroInterno = 'ERRO 00011';
            }
        } else {
            if ($token <> $token_validacao) {                       
                $textoRetorno = 'Sua compra não foi concluída pois sua chave é inválida!';
                $terxtoErroInterno = 'ERRO 00005';
            }   
        }
        

        // VERIFICADNO SE EXISTE SALDO PARA CONTINUAR
        if ($valorSaldo < $valorTotal) {               
            $textoRetorno = 'Saldo insuficiente!';
            $terxtoErroInterno = 'ERRO 00003';
        }      
        

        if (!$textoRetorno) {
            // REALIZA O REGISTRO DA COMPRA e RETIRA DO SALDO
            $dadosCompra = compraRegistrar( $token, $idCarrinho, $id_usuario_pf, $idParceiro, 
                                            /* RETIRADA */                
                                            $tipoAutorRetirada, $nomePessoaRetirada, $tipoDocumentoRetirada, $numeroDocumentoRetirada, 
                                            /* ENTREGA */
                                            $dataEntrega, $nomeBairroEntrega, $nomeComplementoEntrega, $numeroEnderecoEntrega, $nomeEnderecoEntrega, $numeroCepEntrega, $nomeCidadeEntrega, $nomeUFEntrega, $codigoIbgeEntrega,
                                            
                                            $ativoDebitarSaldo, $ativoProducao);
            
            // UMA VEZ QUE A COMPRA SEJA REALIZADA, EFETUAR O RETORNO DA TRANSAÇÃO
            if ($dadosCompra){
                foreach($dadosCompra as $item) {
                    $idCompra = $item['idCompra'];
                    $valorLiquido = $item['valorLiquido'];
                    $idUnidade = $item['idUnidade'];

                    $ativoRetorno = $item['ativoRetorno'];
                    $textoRetorno = $item['textoRetorno'];

                    /* CASO RETORNE O ID DA COMPRA, EXCLUIR O CARRINHO*/
                    if ($ativoRetorno) {
                        $dadosCarrinho = carrinho('D', $idCarrinho, null, $id_usuario_pf);
                    }
                    
                } 

                if ($ativoRetorno) {
                    switch ($tipoTransacao) {
                        case 'cd': // COMPRA DIRETA
                            $ativoCompraRealizada = 1;
                            $textoRetorno = 'COMPRA REALIZADA COM SUCESSO!';
                            break;
                        case 'vc': // VALE COMPRA                            
                            $dadosValeCompra = comprarValeCompra($token, $valorLiquido, $idCompra, $idUnidade);

                            if ((array_key_exists("code", $dadosValeCompra)) && ($dadosValeCompra['code'] == 200)) {   
                                
                                foreach ($dadosValeCompra['data'] as $item){        
                                    $codigoValeCompras = $item['codigo'];   
                                }                                   
                                
                                $ativoCompraRealizada = 1;
                                $textoRetorno = 'VALE COMPRAS GERADO COM SUCESSO!';     
                                //$textoRetorno = $dadosValeCompra['mensagem'];     

                            } else {   
                                // REVERTENDO TRANSAÇÃO EM CASO DE ERRO
                                compraReverter($idCarrinho, $idCompra, $id_usuario_pf, $ativoDebitarSaldo, $ativoProducao, $textoRetorno);                                 

                                $ativoCompraRealizada = 0;
                                $textoRetorno = $dadosValeCompra['mensagem'];   
                                $terxtoErroInterno = 'ERRO 00009';                      
                            }
                            break;
                        /*
                        case 's': // SERVIÇO
                            if ($ativoControleCodigo) {
                                $dadosCodigoProduto = produtoCodigoVincular($idCompra, $idProduto);

                                foreach($dadosCodigoProduto as $item) {
                                    $codigoVoucher = $item['codigoVaucher'];
                                }
                            }

                            $ativoCompraRealizada = 1;
                            $textoRetorno =     '<p>COMPRA REALIZADA COM SUCESSO!</p>';

                            break;
                        
                        case 'g': // GIFTCARD
                            $dadosGiftCard = comprarGiftCard($token, tipoGiftCard($tipoGiftCard), $valorProduto);
                        

                            if ((array_key_exists("code", $dadosGiftCard)) && ($dadosGiftCard['code'] == 200)) {                               
                                $codigoGiftCard = $dadosGiftCard['codigo'];   
                                $ativoCompraRealizada = 1;
                                $textoRetorno = $dadosGiftCard['mensagem'];                            
                            } else {   
                                // REVERTENDO TRANSAÇÃO EM CASO DE ERRO
                                compraReverter($idCarrinho, $idCompra, $id_usuario_pf, $ativoDebitarSaldo, $ativoProducao, $textoRetorno);                         

                                $ativoCompraRealizada = 0;
                                $textoRetorno = $dadosGiftCard['mensagem'];  
                                $terxtoErroInterno = 'ERRO 00008';                      
                            }

                            break;
                        case 'r': // RECARGA                           
                            $dadosRecarga = comprarRecarga($token, 'celular', $valorProduto, $nomeOperadoraRecarga, $numeroCelular);
                            
                            if ((array_key_exists("code", $dadosRecarga)) && ($dadosRecarga['code'] == 200)) {   
                                $codigoGiftCard = $dadosRecarga['codigo'];   

                                $ativoCompraRealizada = 1;
                                $textoRetorno = $dadosRecarga['mensagem'];                                    
                                
                            } else {                         
                                // REVERTENDO TRANSAÇÃO EM CASO DE ERRO
                                    compraReverter($idCarrinho, $idCompra, $id_usuario_pf, $ativoDebitarSaldo, $ativoProducao, $textoRetorno);     

                                $ativoCompraRealizada = 0;
                                $textoRetorno = $dadosRecarga['mensagem'];                            
                                $terxtoErroInterno = 'ERRO 00007';
                            }
                            break;
                            */
                    }
                } else {
                    // REVERTENDO TRANSAÇÃO EM CASO DE ERRO
                    //    compraReverter($idCarrinho, $idCompra, $id_usuario_pf, $ativoDebitarSaldo, $ativoProducao, $textoRetorno);         
                    $ativoCompraRealizada = 0;                        
                    $textoRetorno = 'FALHA NA COMPRA: '.$textoRetorno;    
                    $terxtoErroInterno = 'ERRO 00006';
                }
                
            } else {
                // REVERTENDO TRANSAÇÃO EM CASO DE ERRO                    
                compraReverter($idCarrinho, $idCompra, $id_usuario_pf, $ativoDebitarSaldo, $ativoProducao, $textoRetorno);   
                //
                $textoRetorno = 'Sua compra não pode ser concluída!';
                $terxtoErroInterno = 'ERRO 00001';                    
            }
        }   
        
    } catch (Exception $e) {      
        // REVERTENDO TRANSAÇÃO EM CASO DE ERRO
        compraReverter($idCarrinho, $idCompra, $id_usuario_pf, $ativoDebitarSaldo, $ativoProducao, $textoRetorno);        

        $textoRetorno = 'Uma ação inexperada aconteceu, tente novamente mais tarde!';
        $terxtoErroInterno = 'ERRO 00004'.$e->getMessage();

        include "auditoria/compra.auditoria.erro.php";
    } finally {  
        if ($ativoCompraRealizada){
            include "auditoria/compra.auditoria.compra.php";

        /* $dadosParceiroRetirada = parceiros($idParceiro);

            foreach ($dadosParceiroRetirada as $item) {
                $idUnidadeRetirada = $item['idUnidade'];
            }

            $nomeTipoCompra		=	tipoProduto($tipoProduto);

            $dadosRetorno = enviarDadosCompra($nomeUsuario, $numeroCelularUsuario, $emailUsuario, $nomeProduto, $idUnidadeRetirada, $tipoDocumentoRetirada, $nomePessoaRetirada, $numeroDocumentoRetirada, $nomeTipoCompra);
*/
        } else {
            include "auditoria/compra.auditoria.erro.php";
        }
        // RETORNANDO PARA A TELA INICIAL
        include "retorno/compra.retorno.padrao.php";
    }
?>
