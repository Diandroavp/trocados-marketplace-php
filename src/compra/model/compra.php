<?php
    
    function produtoCodigoVincular($idCompra, $idProduto){
        $connection = abirConnection();   
        $params = array($idCompra, $idProduto);
        $dados = executeSP("spProdutoCodigo_Vincular", $params, $connection);        
        return $dados;
    }


    
    function produtoUsuarioRegistrar($token, $idProduto, $id_usuario_pf){
        $connection = abirConnection();   
        $params = array($token, $idProduto, $id_usuario_pf);
        $dados = executeSP("spProdutoUsuario_Registrar", $params, $connection);        
        return $dados;
    }

    function compraRegistrar($token, $idCarrinho, $id_usuario_pf, $idParceiro, $tipoAutorRetirada, $nomePessoaRetirada, $tipoDocumentoRetirada, $numeroDocumentoRetirada, $dataEntrega, $nomeBairroEntrega, $nomeComplementoEntrega, $numeroEnderecoEntrega, $nomeEnderecoEntrega, 
                            $numeroCepEntrega,$nomeCidadeEntrega, $nomeUFEntrega, $codigoIbgeEntrega, $ativoDebitarSaldo, $ativoProducao){
        
        global $url_comprar_registrar;

        $url = $url_comprar_registrar;

        $data = [
            "idCarrinho"=> (string)$idCarrinho, 
            "id_usuario_pf"=> $id_usuario_pf, 
            "idParceiro"=> $idParceiro, 
            "tipoAutorRetirada"=> $tipoAutorRetirada, 
            "nomePessoaRetirada"=> $nomePessoaRetirada, 
            "tipoDocumentoRetirada"=> $tipoDocumentoRetirada, 
            "numeroDocumentoRetirada"=> $numeroDocumentoRetirada, 
            "dataEntrega"=> $dataEntrega, 
            "nomeBairroEntrega"=> $nomeBairroEntrega,
            "nomeComplementoEntrega"=> $nomeComplementoEntrega,
            "numeroEnderecoEntrega"=> (string)$numeroEnderecoEntrega,
            "nomeEnderecoEntrega"=> $nomeEnderecoEntrega,
            "nomeCidadeEntrega"=> $nomeCidadeEntrega,
            "numeroCepEntrega"=> (string)$numeroCepEntrega,
            "nomeUFEntrega"=> $nomeUFEntrega,
            "codigoIbgeEntrega"=> $codigoIbgeEntrega,
            "ativoDebitarSaldo"=> $ativoDebitarSaldo,
            "ativoProducao"=> $ativoProducao,            
        ]; 

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array('Authorization:'.$token.''));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $resposta = curl_exec($ch);
        curl_close($ch);             
        
        $respostaArray = json_decode($resposta, true);        
        
        if ((array_key_exists("code", $respostaArray)) && ($respostaArray['code'] == 200)) {       
            $dados = $respostaArray['data'][0];
        } else {
            $dados = null;
        }
        
        return $dados;
    }  
    
    function compraReverter($idCarrinho, $idCompra, $id_usuario_pf, $ativoDebitarSaldo, $ativoProducao, $textoEstorno){
        $connection = abirConnection();   
        $params = array((string)$idCarrinho, $idCompra, $id_usuario_pf, $ativoDebitarSaldo, $ativoProducao, $textoEstorno);        
        $dados = executeSP("spCompra_Reverter", $params, $connection);        
        return $dados;
    }  

    function comprarRecarga($token, $tipo, $valor_promocao, $nomeOperadora, $numeroCelular){ 
        
        global $url_compra_recarga;

        $url = $url_compra_recarga;

        $data = []; 
        $data["tipo"] =  $tipo;
        $data["valor_promocao"] =  $valor_promocao;
        $data["operadora"] =  $nomeOperadora;
        $data["celular"] =  $numeroCelular;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array('Authorization:'.$token.''));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $resposta = curl_exec($ch);
        curl_close($ch);             
        
        $respostaArray = json_decode($resposta, true);          

        return $respostaArray;
    }

    

    function comprarGiftCard($token, $tipo, $valor_promocao){ 

        global $url_compra_giftcard;
        
        $url = $url_compra_giftcard;

        $data = []; 
        $data["tipo"] =  $tipo;
        $data["valor_promocao"] =  $valor_promocao;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array('Authorization:'.$token.''));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $resposta = curl_exec($ch);
        curl_close($ch);             
        
        $respostaArray = json_decode($resposta, true);          

        return $respostaArray;
    }

    /**
     * @token Token do usuário
     * @valor_real Valor do Produto com Desconto
     * @valor_black Valor cheio sem desconto
     * @id_unidade Unidade do Parceiro
     *
    */
    function comprarValeCompra($token, $valor_voucher, $idCompra, $id_unidade_compra){ 

        global $url_compra_vale_compra;        
        $url = $url_compra_vale_compra;

        $data = []; 
        $data["valor_voucher"] =  $valor_voucher;                
        $data["idCompra"] =  $idCompra;
        $data["id_unidade_compra"] =  $id_unidade_compra;        

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array('Authorization:'.$token.''));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $resposta = curl_exec($ch);
        curl_close($ch);             
        
        $respostaArray = json_decode($resposta, true);          

        return $respostaArray;
    }

    /* OLD
     function comprarValeCompra($token, $valor_real, $valor_black, $id_unidade){ 

        global $url_compra_vale_compra;        
        $url = $url_compra_vale_compra;

        $data = []; 
        $data["valor_real"] =  $valor_real;
        $data["valor_black"] =  $valor_black;
        $data["id_unidade"] =  $id_unidade;        

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array('Authorization:'.$token.''));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $resposta = curl_exec($ch);
        curl_close($ch);             
        
        $respostaArray = json_decode($resposta, true);          

        return $respostaArray;
    }
     */

    function validarSenha($token, $senha){ 
        
        global $url_validar_senha;
        $url = $url_validar_senha;

        $data = [];         
        $data["senha"] =  $senha;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);         
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array('Authorization:'.$token.''));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $resposta = curl_exec($ch);
        curl_close($ch);             
        
        $respostaArray = json_decode($resposta, true);               
        
        return $respostaArray;
    }

    function tipoGiftCard($tipoGiftCard){        
        $nometipo = '';

        switch($tipoGiftCard) {        
            case "n": /*NETFLIX*/
                $nometipo = 'netflix';                            
                //
                break;
            case "s": /*SPOTIFY*/
                $nometipo = 'spotify';                                  
                //
                break;
            case "g": /*GOOGLE PLAY*/
                $nometipo = 'googleplay';                            
                //
                break;
            case "u": /*UBER*/
                $nometipo = 'uber';                            
                //
                break;
        }

        return $nometipo;
    }

    

    



    function enviarDadosCompra($nomeUsuario, $celular, $email, $nomeProduto, $idUnidade, $tipoDocumento, $nomePessoaRetirada, $numeroDocumento, $tipoCompra){ 
        
        $url = 'https://hooks.slack.com/services/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';
			
		$text 	= 	"nomeUsuario: ".$nomeUsuario;
		$text	=	$text."; celular: ".$celular;
		$text	=	$text."; email: ".$email;
		$text	=	$text."; nomeProduto: ".$nomeProduto;
		$text	=	$text."; idUnidade: ".$idUnidade;
		$text	=	$text."; tipoDocumento: ".$tipoDocumento;
		$text	=	$text."; nomePessoaRetirada: ".$nomePessoaRetirada;
		$text	=	$text."; numeroDocumento: ".$numeroDocumento;
		$text	=	$text."; tipoCompra: ".$tipoCompra;

        $data = []; 
        $data["username"] =  'NOVA COMPRA';
        $data["icon_emoji"] =  ':slack:';
        $data["text"] =  $text;
		
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_SSL_VERIFYPEER => false,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS => json_encode($data),
		  CURLOPT_HTTPHEADER => array(
			'Content-Type: application/json'
		  ),
		));
        

        $resposta = curl_exec($curl);
        curl_close($curl);             
        	      

        return $resposta;
    }

?>
