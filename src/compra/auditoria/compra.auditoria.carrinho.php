<?php 
    //AUDITORIA
    $descricaoAuditoria = 'CarrinhoProduto ['.$idCarrinho.';'.$id_usuario_pf.']';
    //					
    $tipoAuditoria  = 'S';						
    //
    $textoAuditoria = 'Produtos do Carrinho => ';
    $textoAuditoria = $textoAuditoria.'idCarrinho: '.$idCarrinho;
    $textoAuditoria = $textoAuditoria.';dataCarrinho: '.$dataCarrinho;
    $textoAuditoria = $textoAuditoria.';Produtos do Carrinho:';
   
    if ($dadosCarrinhoProduto) {
        foreach ($dadosCarrinhoProduto as $item){
            $textoAuditoria = $textoAuditoria.'{ idProduto: '.$item['idProduto'];
            $textoAuditoria = $textoAuditoria.';numeroQuantidade: '.$item['numeroQuantidade'];
            $textoAuditoria = $textoAuditoria.';valorProduto: '.$item['valorProduto'];
            $textoAuditoria = $textoAuditoria.' }';
        }
    }      
        
    inserirAuditoria($descricaoAuditoria, $tipoAuditoria, $textoAuditoria);
?>