<header class="cabecalho-top">
    <div class=" cabecalho-logo-saldo">
        <div class="row">
            <div class="col-12 ">
                <?php include("cabecalho.endereco.view.php");  ?>
            </div>
        </div>
        <div class="row">
           
                <div class="col-5">
                    <a href="#" onclick="enviaFormularioSimples('frmHome')">
                        <img src="img/logo-branco.svg" alt="" style="width: 100%;  max-width: 250px">
                    </a>
                </div>
                <div class="col-5">
                    <div id="h3header">
                        <?=$nomeUsuarioCurto?>
                    </div>
                    <div id="txtsaldo"><label class="font-18"></label>
                        <?=(isset($valorSaldoFormatado) ? $valorSaldoFormatado : 'R$ 0,00')?>
                    </div>
                </div>
                <div class="col-2">
                    <div id="h3header">
                        <form class="pesquisar" id="frmSair">
                            <a href="#" onclick="return enviaFormularioSimples('frmSair', 0)" class="button-Serch"><i
                                    class="fa fa-qrcode fa-2x"></i></a>
                        </form>
                    </div>
                </div>
            
        </div>
    </div>
    <div class="p-2">
        <?php include("src/buscar/index.php");  ?>
    </div>
</header>
<form role="form" id="frmHome" name="frmHome" action="" method="post">

    <input type="hidden" name="_route" value="" />
</form>