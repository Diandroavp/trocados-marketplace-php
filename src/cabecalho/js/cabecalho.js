  function getUsuarioEndereco(token) {
    var dadosParametro = {    
        token: token,      
        nomeFuncao : 'endereco'
      };

      $.ajax({
      url: './services/usuario/usuario.service.php',
        dataType: "json",
        data: dadosParametro,
        success: function (response) {                          
            $('#divEndereco').html(response[0].div);  
            $('#divFormEndereco').html(response[0].divForm);                                       
        }  
    })
  
  }

