<div id="carrinho" class=" fixarRodape blocoCarrinho" style="display:none;">
    <button onclick="return enviaFormularioSimples('frmCarrinho')" class="btn btn-block"  >
        <div class="row">
            <div class="col-6" style="text-align: left">  
                <span id="quantidadeTotal"></span>                  
                <i class="fa fa-shopping-cart" > </i> 
                Ver carrinho
            </div>
            <div class="col-6">                    
                <span id="valorCarrinhoTotal"></span>
            </div>
        </div>  
    </button>
</div> 

<form id="frmCarrinho" name="frmCarrinho" action="?"> 
    <input type="hidden" name="_route"  value="carrinho" />
    <input type="hidden" name="idParceiroCarrinho" id="idParceiroCarrinho"   />
</form>

<script>
     $(document).ready(function() {
        let token = '<?=$token?>';        
        getCarrinho(token);
    }); 
</script>
