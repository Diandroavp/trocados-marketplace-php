<?php include "endereco.controller.php"?>


<div class="">
    <!--CARD PRINCIPAL-->
    <div class="col-12">        
        <label class="labelCategoria"><b>Meu Endereço</b></label>

        <!-- ENDEREÇO PARA ENTREGA -->
        <?php include "endereco.entrega.php"?>
         

        <form role="form" id="frmEnderecoUsuario" name="frmEnderecoUsuario" action="" method="post" >            
            <input type="hidden" name="_route" id="_route" value="endereco" />                                      
            <input type="hidden" name="tipoAcao" id="tipoAcao" value="E" />    
            <input type="hidden" name="_route_retorno"  value="<?=$_route_retorno?>" /> 
            <input type="hidden" name="idParceiroCarrinho" value="<?=$idParceiroCarrinho?>"/>                                  
        </form>

</div>
<!-- CONFIRMAR COMPRA-->
<div class="blocoConfirmar fixarRodape">
    <div class="row">
        <div class="col-md-12">
            
            <button id="btnConfirmar" type="button" class="btn btn-block btn-lg btnConfirmacao center" >
                Salvar Endereço
            </button>
        </div>
    </div>
</div>   

<script>

    var botao = document.querySelector('#btnConfirmar');

    botao.addEventListener('click', function (event) {
                
        var numeroCep = document.getElementById('numeroCep').value;
        var nomeEndereco = document.getElementById('nomeEndereco').value;
        var nomeBairro = document.getElementById('nomeBairro').value;              
        var nomeComplemento = document.getElementById('nomeComplemento').value;              
        var numeroEndereco = document.getElementById('numeroEndereco').value;              

        if ( (numeroCep == "") || (nomeEndereco == "") || (nomeBairro == "") || (nomeComplemento == "") || (numeroEndereco == "")) {
            Swal.fire({
                icon: 'warning',
                title: 'Oops...',
                confirmButtonColor: '#33CB00',                                
                text: 'Você precisa informar todos os campos de endereço para que a gente não erre o local de entrega!'                
            })
            return false;  
        } else {
            loaderTrocadosJs();
            enviaFormularioSimples('frmEnderecoUsuario');    
            return true;  
        }       
    });
</script>
