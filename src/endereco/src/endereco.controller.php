<?php    
    // ENTREGA
    $tipoAcao = (isset($_REQUEST['tipoAcao'])) ? $_REQUEST['tipoAcao'] : 'S';
    $idEndereco = (isset($_REQUEST['idEndereco'])) ? $_REQUEST['idEndereco'] : null;
    // RETORNO
    $_route_retorno = (isset($_REQUEST['_route_retorno'])) ? $_REQUEST['_route_retorno'] : null;
    $idParceiroCarrinho = (isset($_REQUEST['idParceiroCarrinho'])) ? $_REQUEST['idParceiroCarrinho'] : null;

    
    $nomeEndereco = (isset($_REQUEST['nomeEndereco'])) ? $_REQUEST['nomeEndereco'] : null;
    $numeroEndereco = (isset($_REQUEST['numeroEndereco'])) ? $_REQUEST['numeroEndereco'] : null;
    $nomeBairro = (isset($_REQUEST['nomeBairro'])) ? $_REQUEST['nomeBairro'] : null;
    $nomeComplemento = (isset($_REQUEST['nomeComplemento'])) ? $_REQUEST['nomeComplemento'] : '-';
    $numeroCep = (isset($_REQUEST['numeroCep'])) ? $_REQUEST['numeroCep'] : null;
    $codigoIbge = (isset($_REQUEST['codigoIbge'])) ? $_REQUEST['codigoIbge'] : null;
    
    $numeroCep = str_replace(".", "", $numeroCep);
    $numeroCep = str_replace("-", "", $numeroCep);

    $nomeCidade = (isset($_REQUEST['nomeCidade'])) ? $_REQUEST['nomeCidade'] : null;
    $nomeUF = (isset($_REQUEST['nomeUF'])) ? $_REQUEST['nomeUF'] : null;

    if ($tipoAcao <> 'S') {
        $dadosEndereco = usuarioEndereco($token, $tipoAcao, $idEndereco, $nomeEndereco, $numeroEndereco, $nomeBairro, $nomeComplemento, $numeroCep, $nomeCidade, $nomeUF, $codigoIbge);
        $tipoAcao = 'S';
        include "endereco.controller.resposta.php";
    }
 
    $dadosEndereco = usuarioEndereco($token, $tipoAcao, null, null, null, null, null, null, null, null, null);    

    if (!$dadosEndereco) {
        $dadosEndereco = usuarioEndereco($token, 'I', null, 'CLIQUE AQUI e informe seu endereço.', '000', 'Indefinido', 'Indefinido', '6900000', '-', '-', '000');
        $dadosEndereco = usuarioEndereco($token, $tipoAcao, null, null, null, null, null, null, null, null, null);    
        
    }

    foreach ($dadosEndereco as $item) {
        $idEndereco = $item['idEndereco'];
        $nomeEndereco = $item['nomeEndereco'];
        $numeroEndereco = $item['numeroEndereco'];
        $nomeBairro = $item['nomeBairro'];
        $nomeComplemento = $item['nomeComplemento'];
        $numeroCep = $item['numeroCep'];
        $nomeCidade = $item['nomeCidade'];
        $nomeUF = $item['nomeUF'];
        $codigoIbge = $item['codigoIbge'];


    }
    
?>