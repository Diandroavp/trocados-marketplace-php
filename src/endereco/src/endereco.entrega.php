<div class="" >
	<div class="card-body padding">
		<div class="row">
			<!--<div class="col-12">
				<div class="form-group">
					<label class="verde" for="nomePessoaEntrega">
						Nome Completo
					</label>
					<input type="text" form="frmEnderecoUsuario" class="form-control" id="nomePessoaEntrega"
						name="nomePessoaEntrega" />
				</div>
			</div>
			<div class="col-12">
				<div class="form-group">
					<label class="verde" for="telefonePessoaEntrega">
						Telefone
					</label>
					<input type="text" form="frmEnderecoUsuario" class="form-control" id="telefonePessoaEntrega"
						name="telefonePessoaEntrega" />
				</div>
			</div> -->
			<input type="hidden" form="frmEnderecoUsuario" name="idEndereco" id="idEndereco" value="<?=$idEndereco?>" />   
			<div class="col-12">
				<div class="form-group">
					<label class="verde" for="numeroCep">
						CEP
					</label>
					<input type="text" form="frmEnderecoUsuario" class="form-control" id="numeroCep"
						name="numeroCep" required value="<?=$numeroCep?>" maxlength="9"/>
				</div>
			</div>
			<div class="col-12">
				<div class="form-group">
					<label class="verde" for="nomeEndereco">
						Endereço
					</label>
					<input type="text" form="frmEnderecoUsuario" class="form-control" id="nomeEndereco"
						name="nomeEndereco" required value="<?=$nomeEndereco?>" maxlength="200"/>
				</div>
			</div>
			<div class="col-12">
				<div class="form-group">
					<label class="verde" for="numeroEndereco">
						Número
					</label>
					<input type="text" form="frmEnderecoUsuario" class="form-control" id="numeroEndereco"
						name="numeroEndereco" required value="<?=$numeroEndereco?>" maxlength="6"/>
				</div>
			</div>
			<div class="col-12">
				<div class="form-group">
					<label class="verde" for="nomeComplemento">
						Complemento
					</label>
					<input type="text" form="frmEnderecoUsuario" class="form-control" id="nomeComplemento"
						name="nomeComplemento" required value="<?=$nomeComplemento?>" maxlength="100"/>
				</div>
			</div>
			<div class="col-12">
				<div class="form-group">
					<label class="verde" for="nomeBairro">
						Bairro
					</label>
					<input type="text" form="frmEnderecoUsuario" class="form-control" id="nomeBairro"
						name="nomeBairro" required value="<?=$nomeBairro?>" maxlength="100"/>
				</div>
			</div>
			<div class="col-12">
				<div class="form-group">
					<label class="verde" for="nomeCidade">
						Cidade
					</label>
					<input type="text" form="frmEnderecoUsuario" class="form-control" id="nomeCidade"
						name="nomeCidade" required value="<?=$nomeCidade?>" maxlength="100"/>
					<input type="hidden" form="frmEnderecoUsuario" class="form-control" id="codigoIbge"
						name="codigoIbge" required value="<?=$codigoIbge?>" />
				</div>
			</div>
			<div class="col-12">
				<div class="form-group">
					<label class="verde" for="nomeUF">
						UF
					</label>
					<input type="text" form="frmEnderecoUsuario" class="form-control" id="nomeUF" 
						name="nomeUF" required value="<?=$nomeUF?>" maxlength="2"/>
				</div>
			</div>	
		</div>
	</div>
</div>
 <!-- Adicionando JQuery -->
 <script src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
<script>

        $(document).ready(function() {

            function limpa_formulário_cep() {
                // Limpa valores do formulário de cep.
                $("#nomeEndereco").val("");
                $("#nomeBairro").val("");
                $("#nomeComplemento").val("");
                $("#numeroEndereco").val("");
                $("#nomeCidade").val("");
                $("#nomeUF").val("");
            }
            
            //Quando o campo cep perde o foco.
            $("#numeroCep").blur(function() {

				/* loaderTrocadosJs(); */

                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/\D/g, '');
				

                //Verifica se campo cep possui valor informado.
                if (cep != "") {

                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if(validacep.test(cep)) {                    

                        //Consulta o webservice viacep.com.br/
                        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

							console.log(dados);
							/**MANAUS
							1302603
							ITACOATIARA
							1301902
							Parintins
							1303403
							Rio Preto da Eva
							1303569 */
                            if (!("erro" in dados)) {
								if (dados.ibge == '1302603') {
									$('#dataEntrega').removeAttr('disabled');
								} else {
									$('#dataEntrega').attr('disabled', 'disabled');
								}
							
							//	if ((dados.ibge == '1302603') || (dados.ibge == '1301902') || (dados.ibge == '1303403') || (dados.ibge == '1303569')) {
									//Atualiza os campos com os valores da consulta.
									$("#nomeEndereco").val(dados.logradouro);
									$("#nomeBairro").val(dados.bairro);
									if(dados.complemento != ''){
										$("#nomeComplemento").val(dados.complemento);  
									}else{
										$("#nomeComplemento").val('-');  
									}									
									$("#nomeCidade").val(dados.localidade);
                					$("#nomeUF").val(dados.uf);	   									                       	
                					$("#numeroCep").val(dados.cep);	   									                       	
                					$("#codigoIbge").val(dados.ibge);	   									                       	
							/*
								}  else {
									limpa_formulário_cep();
									Swal.fire({
										icon: 'error',
										title: 'Oops...',
										confirmButtonColor: '#33CB00',
										text: 'Entrega não disponível para sua cidade!!'                
									})
								}*/
                                                                                
                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
                                limpa_formulário_cep();
                                Swal.fire({
									icon: 'error',
									title: 'Oops...',
									confirmButtonColor: '#33CB00',
									text: 'CEP não encontrado!!'                
								})
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
                        limpa_formulário_cep();
						Swal.fire({
									icon: 'error',
									title: 'Oops...',
									confirmButtonColor: '#33CB00',
									text: 'Formato do CEP inválido!!'                
								})
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                }
            });
        });

    </script>