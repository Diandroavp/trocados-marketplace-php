<head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <!--<meta charset="utf-8">-->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--FaveIcon-->
	<link rel="shortcut icon" href="img/icon.ico" type="image/x-icon"/>

        <!-- JQUERY 
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>-->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
        <!-- BOOTSTRAP CSS-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

        <!-- BOOTSTRAP JS-->        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>



        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
        <!-- Optional: include a polyfill for ES6 Promises for IE11 
        <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
        -->


        <!-- ICONES -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
                integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ"
                crossorigin="anonymous">

        <!-- JS e CSS SLIDE TOUCH-->
        <script src="js/slick.min.js"></script>        
        <link href="css/slick.css" rel="stylesheet">

        <!-- JS e CSS CUSTOMIZADO-->
        <?php include "js/funcoes-diretivas.php";?>
        <link href="css/geral.css" rel="stylesheet">
        <link href="css/carrinho.css" rel="stylesheet">

        <!-- MDB5-->        
        <link href="css/MDB5/mdb.min.css" rel="stylesheet">
        
        

        <!--CONFIGURAÇÃO LOADING TROCADOS-->
        <script type="text/javascript"
                src="https://cdnjs.cloudflare.com/ajax/libs/bodymovin/4.13.0/bodymovin.min.js"></script>

        <!-- FONT-FAMILY-->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap" rel="stylesheet">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Merriweather+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,300;1,400;1,500;1,600;1,700;1,800&display=swap"
                rel="stylesheet">

</head>