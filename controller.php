<?php
    global $dadosUsuario, $id_usuario_pf, $valorSaldo, $valorSaldoCoin, $valorSaldoFormatado, $valorSaldoCoinFormatado, $nomeUsuario,  $numeroCelularUsuario, $emailUsuario, $ativo_trocados;
  
    if (isset($_REQUEST['token'])) {    

        $dadosUsuario = usuario($token);         

        if ((array_key_exists("code", $dadosUsuario)) && ($dadosUsuario['code'] == 200)) {        

            foreach ($dadosUsuario['data'] as $item){        
                $valorSaldo                 = $item['saldo'];   
                $valorSaldoFormatado        = formatar_moeda($item['saldo'],2);   
                $valorSaldoCoin             = $item['saldo_coin'];   
                $valorSaldoCoinFormatado    = formatar_tcoin($item['saldo_coin'],2);   
                $id_usuario_pf              = $item['id_usuario_pf'];                
            }
            
            $nomeUsuarioCurto = 'Seu Saldo';

            foreach ($dadosUsuario['usuario'] as $item){        
                $nomeUsuario = $item['nome'];
                $nomeUsuarioCurto = limitarTexto($item['nome'], strpos($item['nome']," ") + 1);                
                $numeroCelularUsuario = $item['celular'];
                $emailUsuario = $item['email'];
                $ativo_trocados = $item['ativo_trocados'];
                //$nomeUsuario = $item['nome'];
                //$numeroCelularUsuario = $item['celular'];
                //$emailUsuario = $item['email'];
            }
            
            include("src/cabecalho/index.php");          
            include("route/route.home.php");   
            //include("src/rodape/index.php");       
        } else {
            include("src/cabecalho/index.php");  
            include("src/erro/usuario.php"); 
            //include("src/rodape/index.php");     
        }
    } else {
        include("src/direciona/index.php");                  
        //include("src/rodape/index.php");    
    }
?>


<?php if ((!$ativo_uso_app) && (isset($_REQUEST['token'])) ) {?>
<script>
    setTimeout(redirecionarQRCode, 360000);

    function redirecionarQRCode() {
        loaderTrocadosJs();
        enviaFormularioSimples('frmSair', 0);
    }
</script>
<?php }?>